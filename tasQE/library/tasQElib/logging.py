import logging
import os

''' Logging:
This module makes logs on functions, where is it connected.
'''

def is_logging_file_isset(path):
    ''' Checks, if logging file isset, and creates if not.
    '''
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))

    try:
        open(path, 'r').close()
    except FileNotFoundError:
        open(path, 'w').close()

def set_logging(is_enabled=True, is_log_all=True, log_file='./log.log', log_format='%(asctime)s, %(name)s, [%(levelname)s]: %(message)s'):
    ''' Initialize logging.
    '''
    is_logging_file_isset(log_file)

    formatter = logging.Formatter(log_format)
    file_handler = logging.FileHandler(log_file)

    if is_log_all:
        file_handler.setLevel(logging.DEBUG)
    else:
        file_handler.setLevel(logging.WARNING)

    file_handler.setFormatter(formatter)

    logger = get_logger()
    logger.setLevel(logging.DEBUG)

    if (logger.hasHandlers()):
        logger.handlers.clear()

    if is_enabled:
        logger.disabled = False
        logger.addHandler(file_handler)
    else:
        logger.disabled = True

def get_logger():
    return logging.getLogger('tasQElib logging')
