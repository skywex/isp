class Subtasks:
    ''' Subtasks class documentation:
    Used for relation tasks to parent-tasks.

    Fields:
       task -- int value, means parent task ID;
       subtask -- int value, means related to parent task ID.
    '''
    def __init__(self, task, subtask):
        self.task = task
        self.subtask = subtask