import datetime
import calendar

SHIFT_VALUES = {
    1: datetime.timedelta(days=1),
    2: datetime.timedelta(days=3),
    3: datetime.timedelta(days=7),
    4: datetime.timedelta(days=14),
    5: datetime.timedelta(days=calendar.monthrange(datetime.date.today().year, datetime.date.today().month)[1])
}

class Period:
    ''' Period class documentation:
    Used for task deadline time-shifting.

    Fields:
       ID -- int value, generated automaticly by database;
       task -- int value, means task ID;
       shift -- int value, means time-shift period (1..5).
    '''
    def __init__(self, task, shift, ID=None):
        self.ID = ID
        self.task = task
        self.shift = shift

    def get_shift(self):
        ''' Converts int time-shift value to datetime.
        '''
        return SHIFT_VALUES.get(self.shift)