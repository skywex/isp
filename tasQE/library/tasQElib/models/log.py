import datetime

class Log():
    ''' Log class documentation:

    Fields:
       ID -- int value, generated automaticly by database;
       project -- int value, means ID of sender-project;
       title -- string value, means log category;
       body -- string value, means log description;
       sender -- int value, means ID of sender-user;
       receiver -- int value, means ID of receiver-user;
       priority -- int value (0: "regular", 1: "regular_colored", 2: "middle", 3: "high", 4: "done");
       creation_time -- datetime value, generated automaticly by database, means datetime of log creation;
       is_deleted -- int value, marks log as archived (0/1).       
    '''
    def __init__(self, title, body, sender, receiver, priority, project=None, is_deleted=0, creation_time=datetime.datetime.now(), ID=None):
        self.ID = ID
        self.project = project
        self.title = title
        self.body = body
        self.sender = sender
        self.receiver = receiver
        self.priority = priority
        self.creation_time = creation_time
        self.is_deleted = is_deleted