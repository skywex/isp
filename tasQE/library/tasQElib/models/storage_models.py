import peewee as db

''' Storage Models:
Here, the application classes are displayed in the form in which they
are stored in the database

'''

database = db.SqliteDatabase('tasQE.db')

class Project(db.Model):
    ID = db.AutoField(primary_key=True)
    title = db.CharField()
    color = db.IntegerField(null=True)
    owner = db.IntegerField()
    creation_time = db.DateTimeField()
    is_deleted = db.IntegerField()

    class Meta: 
        database = database


class Tasklist(db.Model):
    ID = db.AutoField(primary_key=True)
    project = db.IntegerField(null=True)
    title = db.CharField() 
    owner = db.IntegerField()    
    priority = db.IntegerField()    
    nextlist = db.IntegerField(null=True)
    creation_time = db.DateTimeField()
    is_deleted = db.IntegerField()

    class Meta: 
        database = database


class Task(db.Model):
    ID = db.AutoField(primary_key=True)
    title = db.CharField()  
    deadline = db.DateTimeField(null=True)
    owner = db.IntegerField()
    is_deleted = db.IntegerField() 
    edit_time = db.DateTimeField(null=True)
    creation_time = db.DateTimeField()
    period = db.IntegerField()
    status_change_time = db.DateTimeField(null=True)
    task_type = db.IntegerField()
    tasklist = db.IntegerField(null=True) 
    desc = db.TextField(null=True)
    priority = db.IntegerField()
    status = db.IntegerField()      

    class Meta:
        database = database


class User(db.Model):
    ID = db.AutoField(primary_key=True)
    name = db.CharField() 
    username = db.CharField() 
    mail = db.CharField()   
    password = db.CharField()
    creation_time = db.DateTimeField()

    class Meta: 
        database = database


class Session(db.Model):
    ID = db.AutoField(primary_key=True)
    owner = db.IntegerField()
    expire = db.DateTimeField() 

    class Meta:  
        database = database  


class Notification(db.Model):
    ID = db.AutoField(primary_key=True)
    title = db.CharField()    
    body = db.TextField()
    sender = db.IntegerField()
    receiver = db.IntegerField()
    priority = db.IntegerField()
    creation_time = db.DateTimeField() 
    is_deleted = db.IntegerField()

    class Meta: 
        database = database 


class Log(db.Model):
    ID = db.AutoField(primary_key=True) 
    project = db.IntegerField(null=True) 
    title = db.CharField()  
    body = db.TextField()
    sender = db.IntegerField()
    receiver = db.IntegerField()
    priority = db.IntegerField()
    creation_time = db.DateTimeField() 
    is_deleted = db.IntegerField()

    class Meta:  
        database = database

class Relation(db.Model):
    rtype = db.CharField()
    rid = db.IntegerField()
    user = db.IntegerField()

    class Meta:
        database = database

class Period(db.Model):
    ID = db.IntegerField(primary_key=True)
    task = db.IntegerField()
    shift = db.IntegerField()

    class Meta:
        database = database

class Subtasks(db.Model):
    task = db.IntegerField()
    subtask = db.IntegerField()

    class Meta:
        database = database