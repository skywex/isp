import datetime

PRIORITY_VALUES = {
    0: "regular",
    1: "regular_colored",
    2: "middle",
    3: "high",
    4: "done"
}

STATUS_VALUES = {
    0: "in_process",
    1: "done"
}

class Task():
    ''' Task class documentation:

    Fields:
       ID -- int value, generated automaticly by database. Used for task-managing;
       title -- string value;
       deadline -- datetime value, means when task should be completed;
       owner -- int value, means ID of owner-user;
       is_deleted -- int value, marks task as archived (0/1);
       edit_time -- datetime value, means when task was edited;
       creation_time -- datetime value, generated automaticly by database, means datetime of task creation;
       period -- int value, means ID of related to this task deadline time-shift;
       status_change_time -- datetime value, means datetime of task status (complete/uncomplete) changing;
       task_type -- int value, means if task is a subtask (0/1);
       tasklist -- int value, means parent tasklist ID (non-required);
       desc -- string value, means task description;
       priority -- int value (0: "regular", 1: "regular_colored", 2: "middle", 3: "high", 4: "done");
       status -- int value (0: "in process", 1: "completed").       
    '''
    def __init__(self, title, owner, is_deleted=0, deadline=None, edit_time=None, creation_time=datetime.datetime.now(),
                 period=0, status_change_time=None, task_type=0, tasklist=None, desc=None, priority=0,
                 status=0, ID=None):

        self.ID = ID
        self.title = title
        self.deadline = deadline
        self.owner = owner
        self.is_deleted = is_deleted 
        self.edit_time = edit_time
        self.creation_time = creation_time
        self.period = period
        self.status_change_time = status_change_time
        self.task_type = task_type
        self.tasklist = tasklist 
        self.desc = desc
        self.priority = priority
        self.status = status

    def get_priority(self):
        ''' Converts int priority value to string.
        '''
        return PRIORITY_VALUES.get(self.priority)

    def get_status(self):
        ''' Converts int status value to string.
        '''
        return STATUS_VALUES.get(self.status)