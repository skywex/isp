import datetime

class Project:
    ''' Project class documentation:

    Fields:
       ID -- int value, generated automaticly by database. Used for project-managing;
       title -- string value;
       color -- string value, means decorative project color f.e. for web-displaying;
       owner -- int value, means ID of owner-user;
       creation_time -- datetime value, generated automaticly by database, means datetime of project creation;
       is_deleted -- int value, marks project as archived (0/1).
    '''
    def __init__(self, title, owner, creation_time=datetime.datetime.now(), is_deleted=0, color=0, ID=None):           
        self.ID = ID
        self.title = title
        self.color = color
        self.owner = owner
        self.creation_time = creation_time
        self.is_deleted = is_deleted