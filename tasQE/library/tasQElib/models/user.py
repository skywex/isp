import datetime

class User:
    ''' User class documentation:

    Fields:
       ID -- int value, generated automaticly by database. Used for user-managing;
       name -- string value, means users real name;
       username -- string value, means nickname of user, and used f.e. for project-member-managing;
       mail -- string value, format: mail@mail.mail;
       password -- string value;
       creation_time -- datetime value, generated automaticly by database, means datetime of user account creation.
    '''
    def __init__(self, name, username, mail, password, creation_time=datetime.datetime.now(), ID=None):
        self.ID = ID
        self.name = name
        self.username = username
        self.mail = mail
        self.password = password
        self.creation_time = creation_time