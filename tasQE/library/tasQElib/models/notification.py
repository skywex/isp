import datetime

class Notification:
    ''' Notification class documentation:

    Fields:
       ID -- int value, generated automaticly by database;
       title -- string value, means notification category;
       body -- string value, means notification description;
       sender -- int value, means ID of sender-user (0 -- means service);
       receiver -- int value, means ID of receiver-user;
       priority -- int value (0: "regular", 1: "regular_colored", 2: "middle", 3: "high", 4: "done");
       creation_time -- datetime value, generated automaticly by database, means datetime of notification creation;
       is_deleted -- int value, marks notification as archived (0/1).       
    '''
    def __init__(self, title, body, sender, receiver, priority, creation_time=datetime.datetime.now(), is_deleted=0, ID=None):    
        self.ID = ID
        self.title = title
        self.body = body
        self.sender = sender
        self.receiver = receiver
        self.priority = priority
        self.creation_time = creation_time 
        self.is_deleted = is_deleted