class tasQError(Exception):
    def __init__(self):
        pass

class MissedArgumentError(tasQError):
    def __init__(self):
        self.code = 1
        self.name = "MissedArgumentError"
        self.desc = "You did not enter all the arguments of this method."

class WrongPasswordError(tasQError):
    def __init__(self):
        self.code = 2
        self.name = "WrongPasswordError"
        self.desc = "You entered the wrong password."

class TableExistError(tasQError):
    def __init__(self):
        self.code = 3
        self.name = "TableExistError"
        self.desc = "This table already exist."

class UserNotExistError(tasQError):
    def __init__(self):
        self.code = 4
        self.name = "UserNotExistError"
        self.desc = "Such user does not exist."

class NeedAuthError(tasQError):
    def __init__(self):
        self.code = 5 # 7
        self.name = "NeedAuthError"
        self.desc = "You need to log in."

class AlreadyAuthorizedError(tasQError):
    def __init__(self):
        self.code = 6
        self.name = "AlreadyAuthorizedError"
        self.desc = "You are already logged in."

class UserExistError(tasQError):
    def __init__(self):
        self.code = 8
        self.name = "UserExistError"
        self.desc = "User with such values already exist."

class IncorrectValueError(tasQError):
    def __init__(self):
        self.code = 9
        self.name = "IncorrectValueError"
        self.desc = "Try to input correct values."

class IncorrectMailError(tasQError):
    def __init__(self):
        self.code = 10
        self.name = "IncorrectMailError"
        self.desc = "Try to input correct e-mail."

class ProjectExistError(tasQError):
    def __init__(self):
        self.code = 11
        self.name = "ProjectExistError"
        self.desc = "You already have such project."

class NoProjectsError(tasQError):
    def __init__(self):
        self.code = 12
        self.name = "NoProjectsError"
        self.desc = "You have not any project yet."

class DBExistError(tasQError):
    def __init__(self):
        self.code = 13
        self.name = "DBExistError"
        self.desc = "Such database is already created."

class EEError(tasQError):
    def __init__(self):
        self.code = 99
        self.name = "EEError"
        self.desc = "Easter egg :)"

class ProjectNotExistError(tasQError):
    def __init__(self):
        self.code = 14
        self.name = "ProjectNotExistError"
        self.desc = "Such project does not exist."

class ProjectNoAccessError(tasQError):
    def __init__(self):
        self.code = 15
        self.name = "ProjectNoAccessError"
        self.desc = "You dont have access to this project."

class ProjectMemberError(tasQError):
    def __init__(self):
        self.code = 16
        self.name = "ProjectMemberError"
        self.desc = "This user is already project member."

class ProjectNotMemberError(tasQError):
    def __init__(self):
        self.code = 17
        self.name = "ProjectNotMemberError"
        self.desc = "This user is not a project member."

class ProjectOwnerDelError(tasQError):
    def __init__(self):
        self.code = 18
        self.name = "ProjectOwnerDelError"
        self.desc = "You can not remove owner from project."

class TasklistExistError(tasQError):
    def __init__(self):
        self.code = 19
        self.name = "TasklistExistError"
        self.desc = "You already have such tasklist."

class TasklistNotExistError(tasQError):
    def __init__(self):
        self.code = 20
        self.name = "TasklistNotExistError"
        self.desc = "You have not such tasklist."

class TasklistNoAccessError(tasQError):
    def __init__(self):
        self.code = 21
        self.name = "TasklistNoAccessError"
        self.desc = "You dont have access to this tasklist."

class DatetimeValueError(tasQError):
    def __init__(self):
        self.code = 22
        self.name = "DatetimeValueError"
        self.desc = "You input date in wrong format. Use help to see the format."

class PeriodValueError(tasQError):
    def __init__(self):
        self.code = 23
        self.name = "PeriodValueError"
        self.desc = "You input invalid period value for task."

class PriorityValueError(tasQError):
    def __init__(self):
        self.code = 24
        self.name = "PriorityValueError"
        self.desc = "You input invalid priority value for task."

class RelationTaskNotExistError(tasQError):
    def __init__(self):
        self.code = 25
        self.name = "RelationTaskNotExistError"
        self.desc = "Such relation task does not exist."

class AlreadySubtaskError(tasQError):
    def __init__(self):
        self.code = 26
        self.name = "AlreadySubtaskError"
        self.desc = "This task has subtask type."

class DoneTaskError(tasQError):
    def __init__(self):
        self.code = 27
        self.name = "DoneTaskError"
        self.desc = "This task is already completed."

class InvalidSubtaskDeadlineError(tasQError):
    def __init__(self):
        self.code = 28
        self.name = "InvalidSubtaskDeadlineError"
        self.desc = "You can not set subtasks deadline bigger then relative task deadline."

class TaskNoAccessError(tasQError):
    def __init__(self):
        self.code = 29
        self.name = "TaskNoAccessError"
        self.desc = "You dont have access to this task."

class SubtaskTasklistError(tasQError):
    def __init__(self):
        self.code = 30
        self.name = "SubtaskTasklistError"
        self.desc = "You can not add subtask to tasklist."

class TaskNotExistError(tasQError):
    def __init__(self):
        self.code = 31
        self.name = "TaskNotExistError"
        self.desc = "Such task does not exist."

class UnDoneTaskError(tasQError):
    def __init__(self):
        self.code = 32
        self.name = "UnDoneTaskError"
        self.desc = "This task is already uncompleted."

class NoNotificationsError(tasQError):
    def __init__(self):
        self.code = 33
        self.name = "NoNotificationsError"
        self.desc = "You dont have any notification."

class NoLogsError(tasQError):
    def __init__(self):
        self.code = 34
        self.name = "NoLogsError"
        self.desc = "You dont have any logs."