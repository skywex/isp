import unittest
import models.error as errors
import controllers.storage_controller as storage
import controllers.models_controller as models

class TestTaskController(unittest.TestCase):
    def setUp(self):
        storage.DB_NAME = ':memory:'
        storage.database_init()

    def test_app_task_create(self):
        with self.assertRaises(errors.TasklistNoAccessError): 
            status = models.app_task_create("Test", None, None, 1, "desc", 1, None)

    def test_app_task_create_2(self):
        with self.assertRaises(errors.TasklistNotExistError): 
            status = models.app_task_create("Test", None, None, 99, "desc", 1, None)

    def test_app_task_create_3(self):
        with self.assertRaises(errors.AlreadySubtaskError): 
            status = models.app_task_create("Test", None, None, None, "desc", 1, 99)

    def test_app_task_create_4(self):
        with self.assertRaises(errors.TaskNoAccessError): 
            status = models.app_task_create("Test", None, None, None, "desc", 1, 1)

    def test_app_task_create_5(self):
        status = models.app_task_create("Test", None, 0, None, "desc", 1, 31)
        self.assertEqual(status, True)

    def test_app_task_complete(self):
        with self.assertRaises(errors.TaskNotExistError): 
            status = models.app_task_complete(9999)   

    def test_app_task_complete_2(self):
        with self.assertRaises(errors.TaskNoAccessError): 
            status = models.app_task_complete(22) 

    def test_app_task_complete_3(self): 
        status = models.app_task_complete(72) 
        self.assertEqual(status, True)

    def test_app_task_complete_4(self):
        with self.assertRaises(errors.DoneTaskError): 
            status = models.app_task_complete(91)  

    def test_app_task_uncomplete(self):
        status = models.app_task_uncomplete(72)
        self.assertEqual(status, True)

    def test_app_task_uncomplete_2(self):
        with self.assertRaises(errors.TaskNoAccessError): 
            status = models.app_task_uncomplete(2)

            