import unittest
import models.error as errors
import controllers.storage_controller as storage
import controllers.models_controller as models

class TestProjectController(unittest.TestCase):
    def setUp(self):
        storage.DB_NAME = ':memory:'
        storage.database_init()

    def test_app_tasklist_create(self):
        with self.assertRaises(errors.ProjectNotExistError): 
            status = models.app_tasklist_create("TaskList1", 99999, 0, 1)

    def test_app_tasklist_create_2(self):
            with self.assertRaises(errors.ProjectNotMemberError): 
                status = models.app_tasklist_create("TaskList1", 1, 0, 1)

    def test_app_tasklist_create_3(self):
            with self.assertRaises(errors.TasklistExistError): 
                status = models.app_tasklist_create("TaskList1", 3, 0, 1)

    def test_app_tasklists_showall(self):
            with self.assertRaises(errors.ProjectNotExistError): 
                status = models.app_tasklists_showall(2)

    def test_app_tasklists_showall_2(self):
            with self.assertRaises(errors.ProjectNoAccessError): 
                status = models.app_tasklists_showall(1)

    def test_app_tasklists_showall_3(self):
        response = models.app_tasklists_showall(3)
        self.assertEqual(response[0].title, "TaskList1")

    def test_app_tasklists_user_showall(self): 
        response = models.app_tasklists_user_showall()
        self.assertEqual(len(response), 0)

    def test_app_tasklists_edit(self): 
        with self.assertRaises(errors.TasklistNotExistError): 
            response = models.app_tasklists_edit(99999, "KEK", 1, 1)

    def test_app_tasklists_edit_2(self): 
        with self.assertRaises(errors.TasklistNoAccessError): 
            response = models.app_tasklists_edit(1, "EDITED", 1, 1)

    def test_app_tasklists_edit_3(self):  
        response = models.app_tasklists_edit(4, None, 1, 1)
        self.assertEqual(response, 'Nothing was changed.')

    def test_app_tasklist_delete(self): 
        with self.assertRaises(errors.TasklistNotExistError): 
            response = models.app_tasklist_delete(999)

    def test_app_tasklist_delete(self): 
        with self.assertRaises(errors.TasklistNoAccessError): 
            response = models.app_tasklist_delete(1) 

    def test_app_tasklist_show(self):
        with self.assertRaises(errors.TasklistNotExistError): 
            response = models.app_tasklist_show(99) 

    def test_app_tasklist_show_2(self):
        with self.assertRaises(errors.TasklistNoAccessError): 
            response = models.app_tasklist_show(1)

    def test_app_tasklist_show_3(self): 
        response = models.app_tasklist_show(None)
        self.assertEqual(response[0], 'Today tasks')
        self.assertEqual(len(response[1]), 0)

    def test_app_tasklist_show_4(self): 
        response = models.app_tasklist_show(4)
        self.assertEqual(response[0], 'TaskList1')
        self.assertEqual(len(response[1]), 2)