import unittest

def run_tests():
    testmodules = [
        'tasQElib.tests.test_project',
        'tasQElib.tests.test_task',
        'tasQElib.tests.test_tasklist',
        'tasQElib.tests.test_user'
        ]

    suite = unittest.TestSuite()

    for t in testmodules:
        suite.addTests(unittest.defaultTestLoader.loadTestsFromName(t))

    unittest.TextTestRunner().run(suite)


if(__name__ == "tasQElib.tests.run_tests"):
    run_tests()