import models.error as errors
import controllers.storage_controller as storage
import controllers.models_controller as models
import unittest

class TestProjectController(unittest.TestCase):
    def setUp(self):
        storage.DB_NAME = ':memory:'
        storage.database_init()

    def test_app_project_create(self):
        with self.assertRaises(errors.ProjectExistError): 
            status = models.app_project_create("Project1", 0)

    def test_app_project_edit(self):
        with self.assertRaises(errors.ProjectNoAccessError): 
            response = models.app_project_edit(1, "EDITED", "desc", 1)

    def test_app_project_edit_valid(self): 
        response = models.app_project_edit(3, "EDITED", "desc", 1)
        self.assertEqual(response, "Nothing was changed.")

    def test_app_project_edit_invalid(self):
        with self.assertRaises(errors.ProjectNotExistError): 
            response = models.app_project_edit(99999, "EDITED", "desc", 1)

    def test_app_project_showall(self):
        response = models.app_project_showall()
        self.assertEqual(len(response), 2)

    def test_app_project_showall_curr(self):
        response = models.app_project_showall()
        self.assertEqual(response[0].title, "EDITED")

    def test_app_project_delete(self):
        with self.assertRaises(errors.ProjectNotExistError): 
            response = models.app_project_delete(2)

    def test_app_project_delete_1(self):
        with self.assertRaises(errors.ProjectNoAccessError): 
            response = models.app_project_delete(1)

    def test_app_project_member_add(self):
        with self.assertRaises(errors.UserNotExistError): 
            response = models.app_project_member_add(3, "lkdfljflfslkfla")

    def test_app_project_member_add_2(self):
        with self.assertRaises(errors.ProjectNotExistError): 
            response = models.app_project_member_add(939393, "tester33")

    def test_app_project_member_add_3(self):
        with self.assertRaises(errors.ProjectMemberError): 
            response = models.app_project_member_add(3, "tester33")
    
    def test_app_project_member_add_4(self):
        with self.assertRaises(errors.ProjectNoAccessError): 
            response = models.app_project_member_add(1, "tester33")    

    def test_app_project_members_show(self):
        response = models.app_project_members_show(3)
        self.assertEqual(response[0].ID, 3)
        self.assertEqual(response[1].ID, 2)

    def test_app_project_members_show_1(self):
        with self.assertRaises(errors.ProjectNoAccessError): 
            response = models.app_project_members_show(1)

    def test_app_project_members_show_2(self):
        with self.assertRaises(errors.ProjectNotExistError): 
            response = models.app_project_members_show(9999)

    def test_app_project_member_delete(self):
        with self.assertRaises(errors.ProjectNotExistError): 
            response = models.app_project_member_delete(9999, "tester33")

    def test_app_project_member_delete_2(self):
        with self.assertRaises(errors.UserNotExistError): 
            response = models.app_project_member_delete(3, "dasddadsad")

    def test_app_project_member_delete_3(self):
        with self.assertRaises(errors.ProjectOwnerDelError): 
            response = models.app_project_member_delete(3, "tester3")

    def test_app_project_member_delete_4(self):
        with self.assertRaises(errors.ProjectNotMemberError): 
            response = models.app_project_member_delete(3, "SkyWex")

    def test_app_project_member_delete_5(self):
        with self.assertRaises(errors.ProjectNoAccessError): 
            response = models.app_project_member_delete(1, "tester3")
