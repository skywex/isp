"""
tasQElib — a simple application library to manage your task plans.

The library consists of 6 entities: project, tasklist, task, notification, user and logs. All possible methods for working with them are located in tasQElib.controllers.models_controller module.
All entities are located in tasQElib.models module. To store all these entities in database, library uses tasQElib.controllers.storage_controller and peewee database.

To work with any entity, you just need import tasQElib.controllers.models_controller.
Library should know, which user is working with it, cos of this, each method requires 'owner' argument.
owner = (USER_ID, USER_USERNAME)

Some examples,
Creating non-period task with title "test task", priority "middle" and simple description:

    >>> import tasQElib.controllers.models_controller as models
    >>> models.app_task_create(title='test task', deadline=None, period=0, tasklist=None, desc='this is my first task', priority=2, task=None, owner=(1, 'tester33')):

Getting task with ID = 1:

    >>> import tasQElib.controllers.models_controller as models
    >>> task = models.app_task_show(1, (1, 'tester33'))

~ If you want to create subtask, you just need set 'task=PARENT_TASK_ID' in models.app_task_create method.
~ Deadline field has dd/mm/yy hh:ii datetime format.
~ If you want to make a period-task, just set 'period=INT_VALUE' in models.app_task_create method.

~ Possible task period values:
    1 - means everyday task,models_controller as models
    >>> task = models.app_task_show(1, (1, 'tester33'))

~ If you want to create subtask, you just need set 'task=PARENT_TASK_ID' in models.app_task_create method.
~ Deadline field has dd/mm/yy hh:ii datetime format.
~ If you want to make a period-task, just set 'period=INT_VALUE' in models.app_task_create method.

~ Possible task period values:
    1 - means everyday task,
    2 - 3-days task,
    3 - weekly task,
    4 - 2 weeks task,
    5 - month task.

~ Possible task/tasklist priority values:
    0: "regular",
    1: "regular_colored",
    2: "middle",
    3: "high",
    4: "done"

~ Also you should know, that deleted tasks dont disappear forever. You can restore if from archive in a moment.

"""