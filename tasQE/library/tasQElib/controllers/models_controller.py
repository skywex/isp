
import tasQElib.models.user as UserClass
import tasQElib.models.project as ProjectClass
import tasQElib.models.session as SessionClass
import tasQElib.models.relation as RelationClass
import tasQElib.models.tasklist as TasklistClass
import tasQElib.models.task as TaskClass
import tasQElib.models.period as PeriodClass
import tasQElib.models.subtasks as SubtasksClass
import tasQElib.models.notification as NotificationClass
import tasQElib.models.log as LogClass
import tasQElib.controllers.storage_controller as storage
import tasQElib.models.error as errors
import tasQElib.logging as log
import datetime

''' Models Controller:
This module performs different methods with class models of application;

'''

def app_user_settings_update(name, mail, password, owner):
    ''' Modifies user account fields. Defines what user fields were changed.

    Arguments:
      name -- string value
      mail -- string value
      password -- string value
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    user = storage.user_select(Username)
    out_ntnh = 'Nothing were changed.'
    out_name = ''
    out_mail = ''
    out_pass = ''

    if name and user.name != name:
        out_name += "Name was changed."
        log.get_logger().info('User Name was changed.')
    else:
        name = user.name
    if mail and user.mail != mail:
        out_mail += "\nMail was changed."
        log.get_logger().info('User Mail was changed.')
    else:
        mail = user.mail
    if password and user.password != password:
        out_pass += "\nPassword was changed."
        log.get_logger().info('User Password was changed.')
        notification = NotificationClass.Notification(title="Account notification", body="Your password was sucessfully changed.", sender=0, receiver=ID, priority=2)
        storage.notification_insert(notification)
    else:
        password = user.password

    user.name = name
    user.mail = mail
    user.password = password
    storage.user_update(user)

    new_log = LogClass.Log(title="Account log", body="changed its account settings.", sender=ID, receiver=ID, priority=2)
    storage.log_insert(new_log)

    if len(out_name) > 0 or len(out_mail) > 0 or len(out_pass) > 0:
        return out_name + out_mail + out_pass
    else:
        return out_ntnh


def app_project_create(title, color, owner):
    ''' Creates new project, if user dont already have a project with such title.

    Arguments:
      title -- string value (required)
      color -- int value
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    if storage.project_select(title=title, owner=ID) is None:
        storage.project_insert(ProjectClass.Project(title=title, owner=ID, color=color))
        storage.relation_insert(RelationClass.Relation("project", storage.project_select(title=title, owner=ID).ID, ID))
        log.get_logger().info('Project was created.')
        new_log = LogClass.Log(project=storage.project_select(title=title, owner=ID).ID, title="Project log", body="creates a new project: " + title, sender=ID, receiver=ID, priority=2)
        storage.log_insert(new_log)

        return True
    else:
        log.get_logger().error('ProjectExistError was raised.')
        raise errors.ProjectExistError()


def app_project_showall(owner):
    ''' Returns projects, where current user is a member or owner.

    Arguments:
       owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner

    projects = storage.project_select_many(ID)
    if len(projects) == 0:
        log.get_logger().error('NoProjectsError was raised.')
        raise errors.NoProjectsError()
    else: 
        log.get_logger().info('Projects were got from database.') 
        return projects


def app_project_edit(id, title, color, owner):
    ''' Modifies current project. Defines, what fields were changed. If project with new title isset, raises an error.

    Arguments:
      id -- int value (required)
      title -- string value
      color -- string value, project color
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    project = storage.project_select(ID=id)

    if project is None:
        log.get_logger().error('ProjectNotExistError was raised.')
        raise errors.ProjectNotExistError()
    elif project.owner == int(ID):
        out_ntnh = 'Nothing were changed.'
        out_title = ''
        out_color = ''
        new_title = ''

        if title and project.title != title:
            out_title += "Title was changed."
            log.get_logger().info('Project title was changed.')
            new_title = title
            new_log = LogClass.Log(project=id, title="Project log", body="changed a project title: " + project.title + " -> " + title + " in project: " + project.title, sender=ID, receiver=ID, priority=2)
            storage.log_insert(new_log)
        else:
            title = project.title
        if color and project.color != color:
            out_color += "\nColor was changed."
            log.get_logger().info('Project color was changed.')
            new_log = LogClass.Log(project=id, title="Project log", body="changed a project color: " + str(project.color) + " -> " + str(color) + " in project: " + project.title, sender=ID, receiver=ID, priority=2)
            storage.log_insert(new_log)
        else:
            color = project.color
        
        # checks, if project with new_title is already exist;
        if storage.project_select(title=new_title, owner=ID) is None:
            project.title = title
            project.color = color
            storage.project_update(project)
        else:
            log.get_logger().error('ProjectExistError was raised.')
            raise errors.ProjectExistError()        

        if len(out_title) > 0 or len(out_color) > 0:
            return out_title + out_color
        else:
            return out_ntnh
    else:
        log.get_logger().error('ProjectNoAccessError was raised.')
        raise errors.ProjectNoAccessError()
    

def app_project_delete(id, owner):
    ''' Marks project as deleted, but do not deletes it forever.

    Arguments:
      id -- int value (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    project = storage.project_select(ID=id)

    if project is None:
        log.get_logger().error('ProjectNotExistError was raised.')
        raise errors.ProjectNotExistError()
    elif project.owner == int(ID):
        log.get_logger().info('Project was deleted.')
        storage.project_delete(id)
        new_log = LogClass.Log(project=id, title="Project log", body="delete a project: " + project.title, sender=ID, receiver=ID, priority=2)
        storage.log_insert(new_log)
        return True
    else:
        log.get_logger().error('ProjectNoAccessError was raised.')
        raise errors.ProjectNoAccessError()


def app_project_member_add(id, username, owner):  
    ''' Adds a member to project by its username, if current user is a project owner.

    Arguments:
      project -- int value, id of project (required)
      username -- string value (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''  
    ID, Username = owner
    project = storage.project_select(ID=id)

    if project is None:
        log.get_logger().error('ProjectNotExistError was raised.')
        raise errors.ProjectNotExistError()
    elif project.owner == int(ID):
        user = storage.user_select(username)

        if user is None:
            log.get_logger().error('UserNotExistError was raised.')
            raise errors.UserNotExistError()
        else:
            user_id = user.ID
            db_relation = storage.relation_select("project", id, user_id)

            if db_relation is None:
                storage.relation_insert(RelationClass.Relation("project", project.ID, user_id))
                notification = NotificationClass.Notification(title="Project-member notification", body="Your was added to project " + project.title + ".", sender=0, receiver=ID, priority=2)
                storage.notification_insert(notification)
                log.get_logger().info('Project member was added.')
                new_log = LogClass.Log(project=id, title="Project log", body="added a new member: " + username + ", to project: " + project.title, sender=ID, receiver=ID, priority=2)
                storage.log_insert(new_log)

                return True
            else:
                log.get_logger().error('ProjectMemberError was raised.')
                raise errors.ProjectMemberError()
    else:
        log.get_logger().error('ProjectNotExistError was raised.')
        raise errors.ProjectNotExistError()


def app_project_members_show(project_id, owner):  
    ''' Returns all members of current project, if current user is a project owner.

    Arguments:
      project -- int value, id of project (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''  
    ID, Username = owner
    project = storage.project_select(ID=project_id)

    if project is None:
        log.get_logger().error('ProjectNotExistError was raised.')
        raise errors.ProjectNotExistError()    
    else:        
        db_relation = storage.relation_select("project", project_id, ID)

        if db_relation is None:
            log.get_logger().error('ProjectNoAccessError was raised.')
            raise errors.ProjectNoAccessError()
        else:
            log.get_logger().info('Project members were displayed.')
            return storage.project_select_members(project_id)        


def app_project_member_delete(id, username, owner):  
    ''' Deletes a member from project, if current user is a project owner.

    Arguments:
      project -- int value, id of project (required)
      username -- string value (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''  
    ID, Username = owner
    project = storage.project_select(ID=id)

    if project is None:
        log.get_logger().error('ProjectNotExistError was raised.')
        raise errors.ProjectNotExistError()
    elif project.owner == int(ID):
        user = storage.user_select(username)

        if user is None:
            log.get_logger().error('UserNotExistError was raised.')
            raise errors.UserNotExistError()
        else:
            user_id = user.ID
            db_relation = storage.relation_select("project", id, user_id)

            if db_relation:
                if user_id == project.owner:
                    log.get_logger().error('ProjectOwnerDelError was raised.')
                    raise errors.ProjectOwnerDelError()
                else:
                    storage.relation_delete(db_relation)
                    notification = NotificationClass.Notification(title="Project-member notification", body="Your was deleted from project " + project.title + ".", sender=0, receiver=ID, priority=2)
                    storage.notification_insert(notification)
                    log.get_logger().info('Project member was deleted.')
                    new_log = LogClass.Log(project=id, title="Project log", body="delete a member: " + username + ", from project: " + project.title, sender=ID, receiver=ID, priority=2)
                    storage.log_insert(new_log)

                    return True
            else:
                log.get_logger().error('ProjectNotMemberError was raised.')
                raise errors.ProjectNotMemberError()
    else:
        log.get_logger().error('ProjectNoAccessError was raised.')
        raise errors.ProjectNoAccessError()


def app_tasklist_create(title, project, priority, nextlist, owner):
    ''' Creates new tasklist to project, if current user is a project member/owner, or creates user tasklist.

    Arguments:
      title -- string value (required)
      project -- int value, id of relative project
      priority -- int value (0..4)
      nextlist -- int value, child tasklist id
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    if project:
        db_project = storage.project_select(ID=project)

        if db_project is None:
            log.get_logger().error('ProjectNotExistError was raised.')
            raise errors.ProjectNotExistError()
        else:
            db_relation = storage.relation_select("project", project, ID)

            if db_relation is None:
                log.get_logger().error('ProjectNotMemberError was raised.')
                raise errors.ProjectNotMemberError()
            else:
                if storage.tasklist_select(title=title, project=project) is None:
                    storage.tasklist_insert(TasklistClass.Tasklist(title=title, owner=ID, nextlist=nextlist, priority=priority, project=project))    
                    storage.relation_insert(RelationClass.Relation("tasklist", storage.tasklist_select(title=title, owner=ID, project=project).ID, project)) 
                    log.get_logger().info('Tasklist was created.')
                    new_log = LogClass.Log(project=project, title="Tasklist log", body="create a new tasklist: " + title + ", to project: " + db_project.title, sender=ID, receiver=ID, priority=2)
                    storage.log_insert(new_log)

                    return True
                else:
                    log.get_logger().error('TasklistExistError was raised.')
                    raise errors.TasklistExistError()
    else:
        db_tasklist = storage.tasklist_select(title=title, owner=ID)
        if db_tasklist is None:
            storage.tasklist_insert(TasklistClass.Tasklist(title=title, owner=ID, nextlist=nextlist, priority=priority, project=project)) 
            log.get_logger().info('Tasklist was created.')
            new_log = LogClass.Log(title="Tasklist log", body="create a new tasklist: " + title, sender=ID, receiver=ID, priority=2)
            storage.log_insert(new_log)

            return True
        elif db_tasklist.project:
            project_tasklist = storage.tasklist_select(title=title, owner=ID, project=db_tasklist.project)
            
            if project_tasklist is None:
                storage.tasklist_insert(TasklistClass.Tasklist(title=title, owner=ID, nextlist=nextlist, priority=priority, project=project))    
                log.get_logger().info('Tasklist was created.')
                new_log = LogClass.Log(title="Tasklist log", body="create a new tasklist: " + title, sender=ID, receiver=ID, priority=2)
                storage.log_insert(new_log)

                return True
            else:
                log.get_logger().error('TasklistExistError was raised.')
                raise errors.TasklistExistError()
        else:
            log.get_logger().error('TasklistExistError was raised.')
            raise errors.TasklistExistError()


def app_tasklists_showall(project_id, owner):
    ''' Returns all tasklist of current project .

    Arguments:
      project -- int value, id of project
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    project = storage.project_select(ID=project_id)

    if project is None:
        log.get_logger().error('ProjectNotExistError was raised.')
        raise errors.ProjectNotExistError()
    else:
        db_relation = storage.relation_select("project", project_id, ID)

        if db_relation is None:
            log.get_logger().error('ProjectNoAccessError was raised.')
            raise errors.ProjectNoAccessError()
        else:
            log.get_logger().info('Project Tasklists were displayed.')
            tasklists = storage.tasklist_select_many(project_id=project_id)  
            return tasklists


def app_tasklists_user_showall(owner):
    ''' Returns all tasklist of current user.
    
    Arguments:
       owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner

    tasklists = storage.tasklist_select_many(owner=ID) 
    log.get_logger().info('Tasklist was displayed.')

    return tasklists 


def app_tasklists_edit(id, title, priority, nextlist, owner):
    ''' Modifies tasklist. Defines, what tasklist fields were changed. If tasklist with new title isset, raises an error.

    Arguments:
      id -- int value (required)
      title -- string value
      priority -- int value (0..4)
      nextlist -- int value, child tasklist id
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    tasklist = storage.tasklist_select(ID=id)

    if tasklist is None:
        log.get_logger().error('TasklistNotExistError was raised.')
        raise errors.TasklistNotExistError()
    elif tasklist.owner == int(ID):
        out_ntnh = 'Nothing were changed.'
        out_title = ''
        out_priority = ''
        out_nextlist = ''
        new_title = ''

        if title and tasklist.title != title:
            out_title += "Title was changed."
            new_title = title
            log.get_logger().info('Tasklist title was changed.')
            new_log = LogClass.Log(project=tasklist.project, title="Tasklist log", body="change tasklist title: " + tasklist.title + " -> " + title + " in tasklist: " + tasklist.title, sender=ID, receiver=ID, priority=2)
            storage.log_insert(new_log)
        else:
            title = tasklist.title
        if priority and tasklist.priority != priority:
            out_priority += "\nPriority was changed."
            log.get_logger().info('Tasklist priority was changed.')
            new_log = LogClass.Log(project=tasklist.project, title="Tasklist log", body="change tasklist priority: " + str(tasklist.priority) + " -> " + str(priority) + " in tasklist: " + tasklist.title, sender=ID, receiver=ID, priority=2)
            storage.log_insert(new_log)
        else:
            priority = tasklist.priority
        if nextlist and tasklist.nextlist != nextlist:
            out_nextlist += "\nNextlist was changed."
            log.get_logger().info('Tasklist nextlist was changed.')
            new_log = LogClass.Log(project=tasklist.project, title="Tasklist log", body="change tasklist nextlist: " + str(tasklist.nextlist) + " -> " + str(nextlist) + " in tasklist: " + tasklist.title, sender=ID, receiver=ID, priority=2)
            storage.log_insert(new_log)
        else:
            nextlist = tasklist.nextlist
        
        # checks, if tasklist with new_title is already exist;
        db_tasklist = storage.tasklist_select(title=new_title, owner=ID, project=tasklist.project)
        if db_tasklist is None:
            tasklist.title = title
            tasklist.nextlist = nextlist
            tasklist.priority = priority
            storage.tasklist_update(tasklist)
        else:
            log.get_logger().error('TasklistExistError was raised.')
            raise errors.TasklistExistError()               

        if len(out_title) > 0 or len(out_priority) > 0 or len(out_nextlist) > 0:
            return out_title + out_priority + out_nextlist
        else:
            return out_ntnh
    else:
        log.get_logger().error('TasklistNoAccessError was raised.')
        raise errors.TasklistNoAccessError()


def app_tasklist_delete(id, owner):
    ''' Marks tasklist as deleted, if current user is its owner, but not deletes it forever.

    Arguments:
      id -- int value (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    tasklist = storage.tasklist_select(ID=id)

    if tasklist is None:
        log.get_logger().error('TasklistNotExistError was raised.')
        raise errors.TasklistNotExistError()
    elif tasklist.owner == int(ID):
        storage.tasklist_delete(id)
        log.get_logger().info('Tasklist was deleted.')
        new_log = LogClass.Log(project=tasklist.project, title="Tasklist log", body="delete tasklist: " + tasklist.title, sender=ID, receiver=ID, priority=2)
        storage.log_insert(new_log)

        return True
    else:
        log.get_logger().error('TasklistNoAccessError was raised.')
        raise errors.TasklistNoAccessError()


def app_tasklist_show(id, owner):
    ''' Returns tasklist with tasks by its id, if current user have access. If id is None, return Today tasks.

    Arguments:
      id -- int value
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner  

    if id:
        tasklist = storage.tasklist_select(ID=id)

        if tasklist is None:
            log.get_logger().error('TasklistNotExistError was raised.')
            raise errors.TasklistNotExistError()
        else:
            if tasklist.project is None: # checks, if it is non-project tasklist, and current user is owner;
                if tasklist.owner == int(ID):
                    tasklist_tasks = storage.tasks_select_many(id)
                    log.get_logger().info('Tasklist was displayed.')

                    return (tasklist.title, tasklist_tasks)
                else:
                    log.get_logger().error('TasklistNoAccessError was raised.')
                    raise errors.TasklistNoAccessError()
            else: # checks, if current user has access to project-tasklist;
                db_relation = storage.relation_select("project", tasklist.project, ID)

                if db_relation is None:
                    log.get_logger().error('TasklistNoAccessError was raised.')
                    raise errors.TasklistNoAccessError()
                else:
                    tasklist_tasks = storage.tasks_select_many(id)  
                    log.get_logger().info('Tasklist was displayed.')

                    return (tasklist.title, tasklist_tasks)        
    else:
        today_tasks = storage.tasklist_select_today(ID)
        return ("Today tasks", today_tasks)


def is_tasklist_accessible(tasklist_id, owner):
    db_tasklist = storage.tasklist_select(ID=tasklist_id)

    if db_tasklist:
        # checks, if its non-project tasklist, and current user - its owner;
        if db_tasklist.project is None:
            if db_tasklist.owner == int(owner):
                return True
            else:
                log.get_logger().error('TasklistNoAccessError was raised.')
                raise errors.TasklistNoAccessError()
        # checks, if its project tasklist, and current user - its member;
        else: 
            db_relation = storage.relation_select("project", db_tasklist.project, owner)

            if db_relation is None:
                log.get_logger().error('TasklistNoAccessError was raised.')
                raise errors.TasklistNoAccessError()
            else:
                return True 
    else:
        log.get_logger().error('TasklistNotExistError was raised.')
        raise errors.TasklistNotExistError() 


def is_task_accessible(task_id, owner):
    task = storage.task_select(ID=task_id)

    if task:
        if task.tasklist and is_tasklist_accessible(task.tasklist, owner):
            return True
        elif task.owner == owner:
            return True
        elif task.task_type == 1:
            relative_task = storage.task_get_parent(task_id)
            return is_task_accessible(relative_task, owner)
        else:
            log.get_logger().error('TaskNoAccessError was raised.')
            raise errors.TaskNoAccessError()
    else:
        log.get_logger().error('TaskNotExistError was raised.')
        raise errors.TaskNotExistError()


def is_task_valid(task, deadline):
    if task: # checks, if such task exist;                
        if task.task_type == 0: # checks, if such task is not a subtask;                    
            if task.status == 0: # checks, if such task is not completed;                        
                if deadline and task.deadline:
                    if float(deadline.timestamp()) <= float(task.deadline.timestamp()): # checks, subtask deadline is in rel.task deadline;
                        return True                  
                    else:
                        log.get_logger().error('InvalidSubtaskDeadlineError was raised.')
                        raise errors.InvalidSubtaskDeadlineError()
                else:
                    return True
            else:
                log.get_logger().error('DoneTaskError was raised.')
                raise errors.DoneTaskError()
        else:
            log.get_logger().error('AlreadySubtaskError was raised.')
            raise errors.AlreadySubtaskError()
    else:
        log.get_logger().error('RelationTaskNotExistError was raised.')
        raise errors.RelationTaskNotExistError()


def app_task_create(title, deadline, period, tasklist, desc, priority, task, owner):
    ''' Creates new task and relates it to tasklist/task, if fields are not None.

    Arguments:
      title -- string value (required)
      deadline -- string value in format dd/mm/yy hh:ii
      period -- int value (1..5), defines task time-shift
      tasklist -- int value, id of parent tasklist
      desc -- string value, means description of task
      priority -- int value (0..4)
      task -- int value, id of relative task
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    new_task = TaskClass.Task(title=title, deadline=deadline, owner=ID, desc=desc, priority=priority)               
    task_project = None

    if tasklist and is_tasklist_accessible(tasklist, ID):
        new_task.tasklist = tasklist 
        task_project = storage.project_select(ID=storage.tasklist_select(ID=tasklist).project).ID    
               
    if task and is_task_accessible(task, ID):
        relative_task = storage.task_select(ID=task)

        if is_task_valid(relative_task, deadline):
            new_task.task_type = 1

    task_id = storage.task_insert(new_task)

    if not period == 0:
        insered_task = storage.task_select(ID=task_id)
        period_id = storage.period_insert(PeriodClass.Period(task=insered_task.ID, shift=period))
        insered_task.period = period_id
        storage.task_update(insered_task)

    if new_task.task_type == 1:
        storage.subtasks_insert(SubtasksClass.Subtasks(task=task, subtask=task_id))

    log.get_logger().info('Task was created.')
    new_log = LogClass.Log(project=task_project, title="Task log", body="create a new task: " + title, sender=ID, receiver=ID, priority=2)
    storage.log_insert(new_log)

    return True


def app_task_edit(id, title, deadline, period, tasklist, desc, priority, owner):
    ''' Modifies task. Defines, what task fields were changed.

    Arguments:
      title -- string value (required)
      deadline -- string value in format dd/mm/yy hh:ii
      period -- int value (1..5), defines task time-shift
      tasklist -- int value, id of parent tasklist
      desc -- string value, means description of task
      priority -- int value (0..4)
      task -- int value, id of relative task
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    db_task = storage.task_select(ID=id)  

    if is_task_accessible(id, ID):            
        task_id = id
        out_ntnh = 'Nothing were changed.'
        out_title = ''
        out_deadline = ''
        out_period = ''
        out_tasklist = ''
        out_desc = ''
        out_priority = ''
        task_project = None

        if db_task.tasklist:
            task_project = storage.project_select(ID=storage.tasklist_select(ID=db_task.tasklist).project).ID    

        if tasklist and is_tasklist_accessible(tasklist, ID):
            if not db_task.tasklist == tasklist:
                out_tasklist = '\nTasklist was changed.'
                new_log = LogClass.Log(project=task_project, title="Task log", body="change task tasklist: " + str(db_task.tasklist) + " -> " + str(tasklist) + " for task: " + db_task.title, sender=ID, receiver=ID, priority=2)
                storage.log_insert(new_log) 
                db_task.tasklist = tasklist     
                log.get_logger().info('Task tasklist was changed.')                 

        if title:
            if not db_task.title == title:
                out_title = '\nTitle was changed.' 
                new_log = LogClass.Log(project=task_project, title="Task log", body="change task title: " + db_task.title + " -> " + title + " for task: " + db_task.title, sender=ID, receiver=ID, priority=2)
                storage.log_insert(new_log) 
                db_task.title = title 
                log.get_logger().info('Task title was changed.')                

        if deadline:
            if not db_task.deadline == deadline:
                out_deadline = '\nDeadline was changed.'
                new_log = LogClass.Log(project=task_project, title="Task log", body="change task deadline: " + str(db_task.deadline) + " -> " + str(deadline) + " for task: " + db_task.title, sender=ID, receiver=ID, priority=2)
                storage.log_insert(new_log) 
                db_task.deadline = deadline
                log.get_logger().info('Task deadline was changed.')

        if priority:
            if not db_task.priority == priority:
                out_priority = '\nPriority was changed.'
                new_log = LogClass.Log(project=task_project, title="Task log", body="change task priority: " + str(db_task.priority) + " -> " + str(priority) + " for task: " + db_task.title, sender=ID, receiver=ID, priority=2)
                storage.log_insert(new_log) 
                db_task.priority = priority        
                log.get_logger().info('Task priority was changed.')      

        if desc:
            if not db_task.desc == desc:
                out_desc = '\nDescription was changed.'
                new_log = LogClass.Log(project=task_project, title="Task log", body="change task description: " + str(db_task.desc) + " -> " + str(desc) + " for task: " + db_task.title, sender=ID, receiver=ID, priority=2)
                storage.log_insert(new_log) 
                db_task.desc = desc
                log.get_logger().info('Task description was changed.')
        
        if period:
            if db_task.period and not db_task.period == period:
                out_period = '\nPeriod was changed.'
                new_log = LogClass.Log(project=task_project, title="Task log", body="change task period for task: " + str(db_task.title), sender=ID, receiver=ID, priority=2)
                storage.log_insert(new_log) 
                storage.period_delete(db_task.period)
                period_id = storage.period_insert(PeriodClass.Period(task=db_task.ID, shift=period))
                db_task.period = period_id
                log.get_logger().info('Task period was changed.')
            elif not db_task.period:
                out_period = '\nPeriod was changed.'
                new_log = LogClass.Log(project=task_project, title="Task log", body="change task period for task: " + str(db_task.title), sender=ID, receiver=ID, priority=2)
                storage.log_insert(new_log) 
                period_id = storage.period_insert(PeriodClass.Period(task=db_task.ID, shift=period))
                db_task.period = period_id
                log.get_logger().info('Task period was changed.')

        
        db_task.edit_time = datetime.datetime.now()
        storage.task_update(db_task)

        if len(out_title) > 0 or len(out_deadline) > 0 or len(out_period) > 0 or len(out_tasklist) > 0 or len(out_desc) > 0 or len(out_priority) > 0:
            return out_title + out_deadline + out_period + out_tasklist + out_desc + out_priority
        else:
            return out_ntnh


def app_task_deadline_validation(task):
    ''' Checks, if current task is a period task and update its deadline by period-shift.
    '''
    if float(task.deadline.timestamp()) < float(datetime.datetime.now().timestamp()):
        period = storage.period_select(task=task.ID)

        if period:       
            task.deadline += period.get_shift()
            storage.task_update(task)
            task = storage.task_select(ID=task.ID)
            log.get_logger().info('Task deadline was growed by period time-shift.')

            return task
        else:
            return task
    else:
        return task


def app_task_complete(id, owner):
    ''' Marks task as done, if current user has access to it and task is not already completed.

    Arguments:
      id -- int value (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner         
    task = storage.task_select(ID=id)
    task_project = None

    if task and task.tasklist:
        task_project = storage.project_select(ID=storage.tasklist_select(ID=task.tasklist).project).ID    

    if is_task_accessible(id, ID):
        if task.status == 0:
            task.status = 1
            task.status_change_time = datetime.datetime.now()
            storage.task_update(task)
            log.get_logger().info('Task was completed.')
            new_log = LogClass.Log(project=task_project, title="Task log", body="complete task: " + task.title, sender=ID, receiver=ID, priority=2)
            storage.log_insert(new_log)
        else:
            log.get_logger().error('DoneTaskError was raised.')
            raise errors.DoneTaskError()

    return True


def app_task_uncomplete(id, owner):
    ''' Marks task as uncompleted, if current user has access to it and task is not already uncompleted.

    Arguments:
      id -- int value (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner            
    task = storage.task_select(ID=id)
    task_project = None

    if task and task.tasklist:
        task_project = storage.project_select(ID=storage.tasklist_select(ID=task.tasklist).project).ID    
    
    if is_task_accessible(id, ID):
        if task.status == 1:
            task.status = 0
            task.status_change_time = datetime.datetime.now()
            storage.task_update(task)
            log.get_logger().info('Task was uncompleted.')
            new_log = LogClass.Log(project=task_project, title="Task log", body="uncomplete task: " + task.title, sender=ID, receiver=ID, priority=2)
            storage.log_insert(new_log)
        else:
            log.get_logger().error('UnDoneTaskError was raised.')
            raise errors.UnDoneTaskError()

    return True


def app_task_delete(id, owner):
    ''' Marks task as deleted, if current user has access to it, but do not deletes it forever.

    Arguments:
      id -- int value (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner  
    db_task = storage.task_select(ID=id) 
    task_project = None

    if db_task and db_task.tasklist:
        task_project = storage.project_select(ID=storage.tasklist_select(ID=db_task.tasklist).project).ID    

    if is_task_accessible(id, ID):
        storage.task_delete(id)
        log.get_logger().info('Task was deleted.')
        new_log = LogClass.Log(project=task_project, title="Task log", body="delete task: " + db_task.title, sender=ID, receiver=ID, priority=2)
        storage.log_insert(new_log)

    return True  


def app_task_show(id, owner):
    ''' Returns task with its subtasks, if current user has access to it.

    Arguments:
      id -- int value (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner  
    db_task = storage.task_select(ID=id) 
    
    if is_task_accessible(id, ID):
        response = storage.task_select_with_subtasks(id)
        log.get_logger().info('Task was displayed.')

        return response


def app_notification_create(title, body, receiver, priority, owner):
    ''' Creates new notification for receiver.

    Arguments:
      title -- string value (required)
      body -- string value (required)
      receiver -- int value, ID of receiver (required)
      priority -- int value (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner  

    response = storage.notification_insert(NotificationClass.Notification(title=title, body=body, sender=ID, receiver=receiver, priority=priority))
    log.get_logger().info('Notification was created.')

    return response


def app_notification_show_all(owner):
    ''' Returns all owners notifications.

    Arguments:
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner

    notifications = storage.notification_select_many(ID)
    if len(notifications) == 0:
        log.get_logger().error('NoNotificationsError was raised.')
        raise errors.NoNotificationsError()
    else:  
        log.get_logger().info('Notifications were displayed.')

        return notifications


def app_notification_clear_all(owner):
    ''' Marks all owners notifications as archived.

    Arguments:
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner

    response = storage.notification_clear_all(ID) 
    log.get_logger().info('Notifications were deleted.')

    return True


def app_log_create(title, body, project, receiver, priority, owner):
    ''' Creates new log.

    Arguments:
      title -- string value (required)
      body -- string value (required)
      project -- int value, ID of project (non-required)
      receiver -- int value, ID of receiver (required)
      priority -- int value (required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner  

    response = storage.log_insert(LogClass.Log(title=title, body=body, project=project, sender=ID, receiver=receiver, priority=priority))
    log.get_logger().info('Log was created.')

    return response


def app_log_show_all(project, owner):
    ''' Returns all owner/project logs.

    Arguments:
      project -- int value, means project ID (non-required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner
    logs = []

    if project:
        db_relation = storage.relation_select("project", project, ID)
        
        if db_relation is None:
            log.get_logger().error('ProjectNoAccessError was raised.')
            raise errors.ProjectNoAccessError()
        else:
            logs = storage.logs_select_many(project=project, owner=ID) 
    else:
        logs = storage.logs_select_many(owner=ID) 

    if len(logs) == 0:
        log.get_logger().error('NoLogsError was raised.')
        raise errors.NoLogsError()
    else:  
        log.get_logger().info('Logs were displayed.')

        return logs


def app_log_clear_all(project, owner):
    ''' Marks all owner/project logs as archived.

    Arguments:
    project -- int value, means project ID (non-required)
      owner -- tuple, means who call this action: (USER_ID, USER_USERNAME)
    '''
    ID, Username = owner

    if project:
        db_relation = storage.relation_select("project", project, ID)
        
        if db_relation is None:
            log.get_logger().error('ProjectNoAccessError was raised.')
            raise errors.ProjectNoAccessError()
        else:
            logs = storage.log_clear_all(project=project, owner=ID) 
    else:
        logs = storage.log_clear_all(owner=ID) 

    log.get_logger().info('Logs were deleted.')

    return True