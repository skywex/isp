import tasQElib.models.storage_models as dbmodels
import tasQElib.models.session as SessionClass
import tasQElib.models.user as UserClass
import tasQElib.models.project as ProjectClass
import tasQElib.models.relation as RelationClass
import tasQElib.models.tasklist as TasklistClass
import tasQElib.models.period as PeriodClass
import tasQElib.models.task as TaskClass
import tasQElib.models.notification as NotificationClass
import tasQElib.models.log as LogClass
import tasQElib.controllers
import configparser
import datetime
import peewee

''' Storage Controller:
This module works with the application database.
The module allows to insert, delete and receive the object from the database.

'''

DB_NAME = 'library_core/tasQE.db'
CONFIG_NAME = 'console_core/config.ini'

def database_init():
    ''' Database initialization.
    '''
    database = peewee.SqliteDatabase(DB_NAME)
    try:
        dbmodels.Project.create_table()
        dbmodels.Tasklist.create_table()
        dbmodels.Task.create_table()
        dbmodels.User.create_table()
        dbmodels.Notification.create_table()
        dbmodels.Log.create_table()
        dbmodels.Session.create_table()
        dbmodels.Relation.create_table()
        dbmodels.Period.create_table()
        dbmodels.Subtasks.create_table()
    except Exception:
        pass

def database_isset():
    ''' Checks, if database isset, and creates it, if not.
    '''
    try:
        file = open(DB_NAME)
    except Exception:
        database_init()

def cache_get():
    ''' Gets user session info.
    '''
    config = configparser.ConfigParser()
    config.read(CONFIG_NAME)
    ID = config['USER']['ID']
    Username = config['USER']['Username']

    return (ID, Username)

def cache_update(ID, Username):
    ''' Updates user session info.
    '''
    config = configparser.ConfigParser()
    config['USER'] = {'ID': ID, 'Username': Username}
    with open(CONFIG_NAME, 'w') as configable:
        config.write(configable)
    
    return True

def cache_clear():
    ''' Clears user session info.
    '''
    config = configparser.ConfigParser()
    config['USER'] = {'ID': 0, 'Username': ''}
    with open(CONFIG_NAME, 'w') as configable:
        config.write(configable)

    return True

def session_select(ID):
    database_isset()
    try:
        db_session = dbmodels.Session.get(dbmodels.Session.owner == ID)

        if db_session:
            return SessionClass.Session(db_session.owner, db_session.ID, db_session.expire)            
        else:
            return None            
    except:
        return None
    
def session_insert(new_session):
    database_isset()
    dbmodels.Session.create(owner=new_session.owner, expire=new_session.expire)

    return True

def session_delete(ID):
    database_isset()
    for ses in dbmodels.Session.select(dbmodels.Session.owner == ID):
        ses.delete_instance()

    return True

def user_select(username=None):
    database_isset()
    if username is None:
        ID, username = cache_get()

    try:
        db_user = dbmodels.User.get(dbmodels.User.username == username)

        if db_user:
            return UserClass.User(db_user.name, db_user.username, db_user.mail, db_user.password, db_user.creation_time, db_user.ID)   
        else:
            return None
    except:
        return None

def user_insert(new_user):
    database_isset()
    dbmodels.User.create(name=new_user.name, username=new_user.username, mail=new_user.mail, password=new_user.password, creation_time=new_user.creation_time)

    return True

def user_update(updated_user):
    database_isset()
    dbmodels.User.update(name=updated_user.name, mail=updated_user.mail, password=updated_user.password).where(dbmodels.User.ID == updated_user.ID).execute()

    return True

def project_insert(new_project):
    database_isset()
    dbmodels.Project.create(title=new_project.title, color=new_project.color, owner=new_project.owner, creation_time=new_project.creation_time, is_deleted=new_project.is_deleted)

    return True

def project_select(title=None, owner=None, ID=None):
    database_isset()
    try:
        if title and owner:
            db_project = dbmodels.Project.get(dbmodels.Project.title == title, dbmodels.Project.owner == owner)
        
        if ID:
            db_project = dbmodels.Project.get(dbmodels.Project.ID == ID)

        if db_project:
            if db_project.is_deleted == 0:
                return ProjectClass.Project(title=db_project.title, owner=db_project.owner, creation_time=db_project.creation_time, is_deleted=db_project.is_deleted, color=db_project.color, ID=db_project.ID)
            else:
                return None            
        else:
            return None
    except:
        return None

def project_select_many(owner):
    database_isset()
    projects = []
        
    for relation in dbmodels.Relation.select().where(dbmodels.Relation.user == owner, dbmodels.Relation.rtype == "project"):        
        db_project = dbmodels.Project.get(dbmodels.Project.ID == relation.rid)
        if db_project.is_deleted == 0:
            projects.append(ProjectClass.Project(title=db_project.title, owner=db_project.owner, creation_time=db_project.creation_time, is_deleted=db_project.is_deleted, color=db_project.color, ID=db_project.ID))

    return projects

def project_update(updated_project):
    database_isset()
    dbmodels.Project.update(title=updated_project.title, owner=updated_project.owner, color=updated_project.color).where(dbmodels.Project.ID == updated_project.ID).execute()

    return True

def project_delete(deleted_project):
    database_isset()
    dbmodels.Project.update(is_deleted=1).where(dbmodels.Project.ID == deleted_project).execute()

    return True

def project_select_members(project_id):
    database_isset()
    members = []
        
    for relation in dbmodels.Relation.select().where(dbmodels.Relation.rid == project_id, dbmodels.Relation.rtype == "project"):        
        db_user = dbmodels.User.get(dbmodels.User.ID == relation.user)
        members.append(UserClass.User(db_user.name, db_user.username, db_user.mail, db_user.password, creation_time=db_user.creation_time, ID=db_user.ID))

    return members

def relation_insert(new_relation):
    database_isset()
    dbmodels.Relation.create(rtype=new_relation.rtype, rid=new_relation.rid, user=new_relation.user)

    return True

def relation_select(rtype, id, user):
    database_isset()
    try:        
        db_relation = dbmodels.Relation.get(dbmodels.Relation.rtype == rtype, dbmodels.Relation.rid == id, dbmodels.Relation.user == user)
        return RelationClass.Relation(db_relation.rtype, db_relation.rid, db_relation.user)
    except:
        return None

def relation_delete(relation):
    database_isset()
    db_relation = dbmodels.Relation.get(dbmodels.Relation.rtype == relation.rtype, dbmodels.Relation.rid == relation.rid, dbmodels.Relation.user == relation.user)
    db_relation.delete_instance()

    return True

def tasklist_select(title=None, owner=None, ID=None, project=None):
    database_isset()
    try:
        if title and owner:
            db_tasklist = dbmodels.Tasklist.get(dbmodels.Tasklist.title == title, dbmodels.Tasklist.owner == owner)
        
        if ID:
            db_tasklist = dbmodels.Tasklist.get(dbmodels.Tasklist.ID == ID)
        
        if project and title:
            db_tasklist = dbmodels.Tasklist.get(dbmodels.Tasklist.project == project, dbmodels.Tasklist.title == title)

        if title and owner and project:
            db_tasklist = dbmodels.Tasklist.get(dbmodels.Tasklist.project == project, dbmodels.Tasklist.title == title, dbmodels.Tasklist.owner == owner)

        if db_tasklist:
            if db_tasklist.is_deleted == 0:
                return TasklistClass.Tasklist(title=db_tasklist.title, owner=db_tasklist.owner, is_deleted=db_tasklist.is_deleted, creation_time=db_tasklist.creation_time, nextlist=db_tasklist.nextlist, priority=db_tasklist.priority, project=db_tasklist.project, ID=db_tasklist.ID)
            else:
                return None            
        else:
            return None
    except:
        return None

def tasklist_insert(new_tasklist):
    database_isset()
    dbmodels.Tasklist.create(project=new_tasklist.project, title=new_tasklist.title, owner=new_tasklist.owner, priority=new_tasklist.priority, nextlist=new_tasklist.nextlist, creation_time=new_tasklist.creation_time, is_deleted=new_tasklist.is_deleted)

    return True

def tasklist_select_many(project_id=None, owner=None):
    database_isset()
    tasklists = []
        
    if project_id:
        for relation in dbmodels.Relation.select().where(dbmodels.Relation.user == project_id, dbmodels.Relation.rtype == "tasklist"):        
            db_tasklist = dbmodels.Tasklist.get(dbmodels.Tasklist.ID == relation.rid)
            if db_tasklist.is_deleted == 0:
                tasklists.append(TasklistClass.Tasklist(title=db_tasklist.title, owner=db_tasklist.owner, is_deleted=db_tasklist.is_deleted, creation_time=db_tasklist.creation_time, nextlist=db_tasklist.nextlist, priority=db_tasklist.priority, project=db_tasklist.project, ID=db_tasklist.ID))
    elif owner:        
        for tasklist in dbmodels.Tasklist.select().where(dbmodels.Tasklist.owner == owner, dbmodels.Tasklist.project == None):
            if tasklist.is_deleted == 0:
                tasklists.append(TasklistClass.Tasklist(title=tasklist.title, owner=tasklist.owner, is_deleted=tasklist.is_deleted, creation_time=tasklist.creation_time, nextlist=tasklist.nextlist, priority=tasklist.priority, project=tasklist.project, ID=tasklist.ID))
    
    return tasklists

def tasklist_select_today(owner):
    database_isset()
    tasks = []        
            
    for task in dbmodels.Task.select().where(dbmodels.Task.owner == owner, dbmodels.Task.is_deleted == 0):
        if task.deadline:
            task = controllers.models_controller.app_task_deadline_validation(TaskClass.Task(title=task.title, deadline=task.deadline, owner=task.owner, is_deleted=task.is_deleted, edit_time=task.edit_time,
                                                                      creation_time=task.edit_time, period=task.period, status_change_time=task.status_change_time, task_type=task.task_type,
                                                                      tasklist=task.tasklist, desc=task.desc, priority=task.priority, status=task.status, ID=task.ID))
            if task.deadline.date() == datetime.datetime.now().date():
                tasks.append(task)            

    return tasks

def tasklist_update(updated_tasklist):
    database_isset()
    dbmodels.Tasklist.update(title=updated_tasklist.title, priority=updated_tasklist.priority, nextlist=updated_tasklist.nextlist).where(dbmodels.Tasklist.ID == updated_tasklist.ID).execute()

    return True

def tasklist_delete(deleted_tasklist):
    database_isset()
    dbmodels.Tasklist.update(is_deleted=1).where(dbmodels.Tasklist.ID == deleted_tasklist).execute()

    return True

def task_insert(new_task):
    database_isset()

    return dbmodels.Task.create(title=new_task.title, deadline=new_task.deadline, owner=new_task.owner, is_deleted=new_task.is_deleted, edit_time=new_task.edit_time, creation_time=new_task.creation_time, period=new_task.period, status_change_time=new_task.status_change_time, task_type=new_task.task_type, tasklist=new_task.tasklist, desc=new_task.desc, priority=new_task.priority, status=new_task.status).ID

def task_select(title=None, owner=None, ID=None):
    database_isset()
    try:
        if ID:
            db_task = dbmodels.Task.get(dbmodels.Task.ID == ID)
        
        if db_task.is_deleted == 0:
            return TaskClass.Task(title=db_task.title, deadline=db_task.deadline, owner=db_task.owner, is_deleted=db_task.is_deleted, edit_time=db_task.edit_time, creation_time=db_task.creation_time,
                 period=db_task.period, status_change_time=db_task.status_change_time, task_type=db_task.task_type, tasklist=db_task.tasklist, desc=db_task.desc, priority=db_task.priority,
                 status=db_task.status, ID=db_task.ID)
        else:
            return None
    except:
        return None

def tasks_select_many(tasklist):
    database_isset()
    tasklist_tasks = []
        
    for db_task in dbmodels.Task.select().where(dbmodels.Task.tasklist == tasklist, dbmodels.Task.is_deleted == 0): 
        tasklist_tasks.append(TaskClass.Task(title=db_task.title, deadline=db_task.deadline, owner=db_task.owner, is_deleted=db_task.is_deleted, edit_time=db_task.edit_time, creation_time=db_task.creation_time, 
                                             period=db_task.period, status_change_time=db_task.status_change_time, task_type=db_task.task_type, tasklist=db_task.tasklist, desc=db_task.desc, priority=db_task.priority,
                                             status=db_task.status, ID=db_task.ID))
    
    return tasklist_tasks

def task_select_with_subtasks(ID):
    database_isset()
    relative_task = dbmodels.Task.get(dbmodels.Task.ID == ID)
    relative_task = TaskClass.Task(title=relative_task.title, deadline=relative_task.deadline, owner=relative_task.owner, is_deleted=relative_task.is_deleted, edit_time=relative_task.edit_time, creation_time=relative_task.creation_time, 
                                           period=relative_task.period, status_change_time=relative_task.status_change_time, task_type=relative_task.task_type, tasklist=relative_task.tasklist, desc=relative_task.desc, priority=relative_task.priority,
                                           status=relative_task.status, ID=relative_task.ID)
    
    subtasks_array = []
    for subtasks in dbmodels.Subtasks.select().where(dbmodels.Subtasks.task == ID):        
        db_task = dbmodels.Task.get(dbmodels.Task.ID == subtasks.subtask)
        if db_task.is_deleted == 0:
            subtasks_array.append(TaskClass.Task(title=db_task.title, deadline=db_task.deadline, owner=db_task.owner, is_deleted=db_task.is_deleted, edit_time=db_task.edit_time, creation_time=db_task.creation_time, 
                                           period=db_task.period, status_change_time=db_task.status_change_time, task_type=db_task.task_type, tasklist=db_task.tasklist, desc=db_task.desc, priority=db_task.priority,
                                           status=db_task.status, ID=db_task.ID))

    return (relative_task, subtasks_array)     

def task_update(updated_task):
    database_isset()
    dbmodels.Task.update(title=updated_task.title, owner=updated_task.owner, deadline=updated_task.deadline, is_deleted=updated_task.is_deleted,
                             edit_time=updated_task.edit_time, period=updated_task.period, status_change_time=updated_task.status_change_time,
                             task_type=updated_task.task_type, tasklist=updated_task.tasklist, desc=updated_task.desc, priority=updated_task.priority,
                             status=updated_task.status).where(dbmodels.Task.ID == updated_task.ID).execute()
    return True

def task_force_delete(task_id):
    database_isset()
    db_task = dbmodels.Task.get(dbmodels.Task.ID == task_id)
    db_task.delete_instance()

    return True

def task_delete(deleted_task):
    database_isset()
    dbmodels.Task.update(is_deleted=1).where(dbmodels.Task.ID == deleted_task).execute()

    return True

def task_get_parent(task_id):
    database_isset()
    db_task = dbmodels.Subtasks.get(dbmodels.Subtasks.subtask == task_id)

    if db_task:
        return db_task.task
    else:
        return None

def period_select(task=None, shift=None, ID=None):
    database_isset()
    try:
        if ID:
            db_period = dbmodels.Period.get(dbmodels.Period.ID == ID)
        elif task and shift:
            db_period = dbmodels.Period.get(dbmodels.Period.task == task, dbmodels.Period.shift == shift)
        elif task:
            db_period = dbmodels.Period.get(dbmodels.Period.task == task)

        if db_period:
            return PeriodClass.Period(ID=db_period.ID, task=db_period.task, shift=db_period.shift)
        else:
            return None
    except:
        return None

def period_delete(ID):
    database_isset()
    db_period = dbmodels.Period.get(dbmodels.Period.ID == ID)
    db_period.delete_instance()
    
    return True

def period_insert(new_period):
    database_isset()
    dbmodels.Period.create(task=new_period.task, shift=new_period.shift)
    return period_select(task=new_period.task, shift=new_period.shift).ID

def subtasks_insert(new_subtasks):
    database_isset()    
    dbmodels.Subtasks.create(task=new_subtasks.task, subtask=new_subtasks.subtask)
    return True

def notification_insert(new_notification):
    database_isset()    
    dbmodels.Notification.create(title=new_notification.title, body=new_notification.body, sender=new_notification.sender, 
                                 receiver=new_notification.receiver, priority=new_notification.priority, creation_time=new_notification.creation_time,
                                 is_deleted=new_notification.is_deleted)
    return True

def notification_select_many(owner):
    database_isset() 
    notifications = []

    for notification in dbmodels.Notification.select().where(dbmodels.Notification.receiver == owner):        
        if notification.is_deleted == 0:
            notifications.append(NotificationClass.Notification(title=notification.title, body=notification.body, sender=notification.sender, receiver=notification.receiver, priority=notification.priority,
                                                                creation_time=notification.creation_time, is_deleted=notification.is_deleted, ID=notification.ID))

    return notifications

def notification_clear_all(owner):
    database_isset()

    for notification in dbmodels.Notification.select().where(dbmodels.Notification.receiver == owner):        
        dbmodels.Notification.update(is_deleted=1).where(dbmodels.Notification.ID == notification.ID).execute()

    return True

def log_insert(new_log):
    database_isset()    
    dbmodels.Log.create(title=new_log.title, body=new_log.body, sender=new_log.sender, receiver=new_log.receiver, priority=new_log.priority, project=new_log.project, is_deleted=new_log.is_deleted, creation_time=new_log.creation_time)
    return True

def logs_select_many(project=None, owner=None):
    database_isset() 
    logs = []

    if project:
        for log in dbmodels.Log.select().where(dbmodels.Log.project == project):        
            if log.is_deleted == 0:
                logs.append(LogClass.Log(title=log.title, body=log.body, sender=log.sender, receiver=log.receiver, priority=log.priority, project=log.project, is_deleted=log.is_deleted, creation_time=log.creation_time, ID=log.ID))
    else:
        for log in dbmodels.Log.select().where(dbmodels.Log.receiver == owner):        
            if log.is_deleted == 0:
                logs.append(LogClass.Log(title=log.title, body=log.body, sender=log.sender, receiver=log.receiver, priority=log.priority, project=log.project, is_deleted=log.is_deleted, creation_time=log.creation_time, ID=log.ID))

    return logs

def log_clear_all(project=None, owner=None):
    database_isset()

    if project:
        for log in dbmodels.Log.select().where(dbmodels.Log.project == project):        
            dbmodels.Log.update(is_deleted=1).where(dbmodels.Log.ID == log.ID).execute()
    else:
        for log in dbmodels.Log.select().where(dbmodels.Log.receiver == owner):        
            dbmodels.Log.update(is_deleted=1).where(dbmodels.Log.ID == log.ID).execute()

    return True