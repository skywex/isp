from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='tasQE Library',
    version='0.0.7',
    description='a simple application to manage your task plans',
    packages=find_packages(),
    install_requires=['peewee'],
    test_suite='tasQElib.tests.run_tests'
)