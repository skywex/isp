import datetime

''' Output Controller:
This module receives a method response and catch the errors.
If an error occurs during the execution of the method, then its text will be displayed.

'''

def show_error(error):
    ''' Prints catched error name.
    '''
    print("[!]", error.name, "was catched.")

def show_error_msg(error):
    ''' Prints catched error description.
    '''
    print("[!]", error.desc)

def show_error_details(error):
    ''' Prints catched error full description.
    '''
    print("[!]", error.name, "was catched.")
    print("Error code:", error.code)
    print("Error description:", error.desc)

def show_projects_all(projects):
    ''' Prints user project info.
    '''
    print(" SHOW ALL PROJECTS:")
    print("==============================")
    for project in projects:
        print(str(project.ID) + ")", project.title, "\t|", project.creation_time.strftime('created: %d %b %Y, in %X'))
    
def show_project_members(members):
    ''' Prints user project memebers.
    '''
    print(" SHOW PROJECT MEMBERS:")
    print("==============================")

    for member in members:
        print("ID:", member.ID)
        print("Username:", member.username)
        print("Name:", member.name)
        print("E-mail:", member.mail)
        print("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~")

def show_user_profile(user):
    ''' Prints user profile info.
    '''
    print(" SHOW USER PROFILE:")
    print("==============================")

    print("ID:", user.ID)
    print("Username:", user.username)
    print("Name:", user.name)
    print("E-mail:", user.mail)

def show_response(response):
    ''' Prints cathed methods answers.
    '''
    if response == True:
        print("Done!")
    elif response == False:
        print("[x] Something went wrong..")
    elif response == None:
        print("[x] Something went wrong..")
    else:
        print(response)

def show_project_tasklists(response):
    ''' Prints user project tasklists.
    '''
    print(" SHOW PROJECT TASKLISTS:")
    print("==============================")

    if len(response) == 0:
        print("Nothing to show.")
    else:
        for tasklist in response:
            print(str(tasklist.ID) + ")", tasklist.title, "\t |", tasklist.creation_time.strftime('created: %d %b %Y, in %X'))

def show_tasklist(response):
    ''' Prints tasklist info and its tasks.
    '''
    print(" SHOW TASKLIST:")
    print("==============================")

    tasklist_title, tasks = response

    print(tasklist_title + ":")
    for task in tasks:
        id = task.ID        
        title = task.title
        desc = task.desc

        overdue = False
        deadline = ""
        if task.deadline:
            if float(task.deadline.timestamp()) < float(datetime.datetime.now().timestamp()):
                overdue = True
            deadline = task.deadline.strftime('%d %b %Y, in %X')  

        status = task.get_status()
        if status == "in_process":
            status = "[~]"
        else:
            status = "[+]"

        priority = task.get_priority()
        if priority == "regular":
            priority = ""
        elif priority == "regular_colored":
            priority = "(c)"
        elif priority == "middle":
            priority = "(•)"
        elif priority == "high":
            priority = "(• • •)"
        

        print(str(id) + ")", status, title, priority)
        print("OVERDUE:", overdue)
        print("DESCRIPTION:", desc)
        if not deadline == "":
            print("DEADLINE:", deadline)
        print("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~")

def show_task(response):
    ''' Prints task info and its subtasks.
    '''
    print(" SHOW TASK:")
    print("==============================")

    task, subtasks_array = response

    id = task.ID        
    title = task.title
    desc = task.desc

    overdue = False
    deadline = ""
    if task.deadline:
        if float(task.deadline.timestamp()) < float(datetime.datetime.now().timestamp()):
            overdue = True
        deadline = task.deadline.strftime('%d %b %Y, in %X')  

    status = task.get_status()
    if status == "in_process":
        status = "[~]"
    else:
        status = "[+]"

    priority = task.get_priority()
    if priority == "regular":
        priority = ""
    elif priority == "regular_colored":
        priority = "(c)"
    elif priority == "middle":
        priority = "(•)"
    elif priority == "high":
        priority = "(• • •)"
    

    print(str(id) + ")", status, title, priority)
    print("OVERDUE:", overdue)
    print("DESCRIPTION:", desc)
    if not deadline == "":
        print("DEADLINE:", deadline)
    print("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~")
    print("SUBTASKS:")
    for task in subtasks_array:
        print("-", task.title)

def show_notifications(response):
    ''' Prints all notifications.
    '''
    print(" SHOW NOTIFICATIONS:")
    print("==============================") 

    for notification in response:
        print(notification.title, ":", notification.body)
        print(notification.creation_time.strftime('%d %b %Y, in %X'))
        print("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~")

def show_logs(response):
    ''' Prints all user/project logs.
    '''
    print(" SHOW LOGS:")
    print("==============================") 

    for log in response:
        print(log.title, ":", log.body)
        print(log.creation_time.strftime('%d %b %Y, in %X'))
        print("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~")
