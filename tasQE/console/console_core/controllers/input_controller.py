import console_core.controllers.output_controller as output
import console_core.controllers.auth_controller as auth
import tasQElib.controllers.storage_controller as storage
import tasQElib.controllers.models_controller as models
import tasQElib.models.error as errors
import re
import datetime

''' Input Controller:
This module receives raw arguments from the console, processes them, and sends them to the application's logic method.
The response received from the method is sent to output_controller.py

'''

def auth_controller_method(tasqe_function):
    ''' Input methods decorator.
    Checks user auth. session. If user is logged in, gives access to methods.

    '''
    def tasqe_function_decorator(*args, **kwargs):
        try:
            if auth.auth_check():
                tasqe_function(*args, **kwargs)
            else:
                raise errors.NeedAuthError()
        except errors.tasQError as e:
            output.show_error_msg(e)

    return tasqe_function_decorator


@auth_controller_method
def project_all_method(args):
    owner = storage.cache_get()
    response = models.app_project_showall(owner)        
    output.show_projects_all(response)


@auth_controller_method
def project_create_method(args):
    owner = storage.cache_get()
    title = args.title
    color = args.color

    if not is_input_correct(title):
        raise errors.IncorrectValueError()
    else:
        response = models.app_project_create(title, color, owner)

    output.show_response(response)


@auth_controller_method
def project_edit_method(args):
    owner = storage.cache_get()
    id = args.id
    title = args.title
    color = args.color

    if args.title and not is_input_correct(args.title):
        raise errors.IncorrectValueError()
    else:
        response = models.app_project_edit(id, title, color, owner)
        
    output.show_response(response)


@auth_controller_method
def project_delete_method(args):
    owner = storage.cache_get()
    id = args.id
    response = models.app_project_delete(id, owner)

    output.show_response(response)


@auth_controller_method
def project_members_add_method(args):
    owner = storage.cache_get()
    project = args.project
    username = args.username

    if not is_input_correct(username):
        raise errors.IncorrectValueError()
    else:
        response = models.app_project_member_add(project, username, owner)

    output.show_response(response)


@auth_controller_method
def project_members_method(args):
    owner = storage.cache_get()
    project = args.project
    response = models.app_project_members_show(project, owner)
    
    output.show_project_members(response)


@auth_controller_method
def project_members_delete_method(args):
    owner = storage.cache_get()
    project = args.project
    username = args.username

    if not is_input_correct(username):
        raise errors.IncorrectValueError()
    else:
        response = models.app_project_member_delete(project, username, owner)

    output.show_response(response)


@auth_controller_method
def tasklist_all_method(args):
    owner = storage.cache_get()
    project = args.project

    if project:
        response = models.app_tasklists_showall(project, owner)
    else:
        response = models.app_tasklists_user_showall(owner)

    output.show_project_tasklists(response)


@auth_controller_method
def tasklist_create_method(args):
    owner = storage.cache_get()
    title = args.title
    project = args.project
    priority = args.priority
    nextlist = args.nextlist

    if not is_input_correct(title):
        raise errors.IncorrectValueError()
    else:
        response = models.app_tasklist_create(title, project, priority, nextlist, owner)
    
    output.show_response(response)


@auth_controller_method
def tasklist_edit_method(args):
    owner = storage.cache_get()
    id = args.id
    title = args.title
    priority = args.priority
    nextlist = args.nextlist

    if args.title and not is_input_correct(args.title):
        raise errors.IncorrectValueError()
    else:
        response = models.app_tasklists_edit(id, title, priority, nextlist, owner)

    output.show_response(response)


@auth_controller_method
def tasklist_delete_method(args):
    owner = storage.cache_get()
    id = args.id
    response = models.app_tasklist_delete(id, owner)
    
    output.show_response(response)


@auth_controller_method
def tasklist_show_method(args):
    owner = storage.cache_get()
    id = args.id
    response = models.app_tasklist_show(id, owner)
    
    output.show_tasklist(response)


@auth_controller_method
def task_create_method(args):
    owner = storage.cache_get()
    title = args.title
    deadline = args.deadline
    period = args.period
    tasklist = args.tasklist
    desc = args.desc
    priority = args.priority
    task = args.task

    if (title and not is_input_correct(title)) or (desc and not is_input_correct(desc)):
        raise errors.IncorrectValueError()
    elif deadline and not is_datetime_correct(deadline):
        raise errors.DatetimeValueError()
    elif not is_period_correct(period):
        raise errors.PeriodValueError()
    elif not is_priority_correct(priority):
        raise errors.PriorityValueError()
    elif tasklist and task:
        raise errors.SubtaskTasklistError()
    else:
        if deadline is None:
            response = models.app_task_create(title, None, period, tasklist, desc, priority, task, owner)
        else:
            datetime_object = datetime.datetime.strptime(deadline, '%d/%m/%y in %H:%M')
            response = models.app_task_create(title, datetime_object, period, tasklist, desc, priority, task, owner)
    
    output.show_response(response)


@auth_controller_method
def task_edit_method(args):
    owner = storage.cache_get()
    id = args.id
    title = args.title
    deadline = args.deadline
    period = args.period
    tasklist = args.tasklist
    desc = args.desc
    priority = args.priority

    if (title and not is_input_correct(title)) or (desc and not is_input_correct(desc)):
        raise errors.IncorrectValueError()
    elif deadline and not is_datetime_correct(deadline):
        raise errors.DatetimeValueError()
    elif period and not is_period_correct(period):
        raise errors.PeriodValueError()
    elif priority and not is_priority_correct(priority):
        raise errors.PriorityValueError()
    else:
        if deadline is None:
            response = models.app_task_edit(id, title, None, period, tasklist, desc, priority, owner)
        else:
            datetime_object = datetime.datetime.strptime(deadline, '%d/%m/%y in %H:%M')
            response = models.app_task_edit(id, title, datetime_object, period, tasklist, desc, priority, owner)

    output.show_response(response)


@auth_controller_method
def task_delete_method(args):
    owner = storage.cache_get()
    id = args.id
    response = models.app_task_delete(id, owner)
    
    output.show_response(response)


@auth_controller_method
def task_done_method(args):
    owner = storage.cache_get()
    id = args.id
    response = models.app_task_complete(id, owner)
    
    output.show_response(response)


@auth_controller_method
def task_undone_method(args):
    owner = storage.cache_get()
    id = args.id
    response = models.app_task_uncomplete(id, owner)
    
    output.show_response(response)


@auth_controller_method
def task_show_method(args):
    owner = storage.cache_get()
    id = args.id
    response = models.app_task_show(id, owner)
    
    output.show_task(response)


def registration_method(args):
    try:
        name = args.name.strip()
        username = args.username.strip()
        mail = args.mail.strip()
        password = args.password.strip()

        if is_input_correct(name) and is_input_correct(username) and is_input_correct(mail) and is_input_correct(password):
            if is_mail_correct(mail):
                response = auth.registration(name, username, mail, password)
            else:
                raise errors.IncorrectMailError()
        else:
            raise errors.IncorrectValueError()
                
        output.show_response(response)
    except errors.tasQError as e:
        output.show_error_msg(e)


def login_method(args):
    try:
        username = args.username.strip()
        password = args.password.strip()
        response = auth.login(username, password)
                
        output.show_response(response)
    except errors.tasQError as e:
        output.show_error_msg(e)


def logout_method(args):
    try:
        if auth.logout():
            response = True
        else:
            raise errors.NeedAuthError()
                
        output.show_response(response)
    except errors.tasQError as e:
        output.show_error_msg(e)


@auth_controller_method
def user_settings_method(args):
    owner = storage.cache_get()
    name = args.name
    mail = args.mail
    password = args.password
    
    if args.mail and not is_mail_correct(args.mail):
        raise errors.IncorrectMailError()
    elif args.name and not is_input_correct(args.name):
        raise errors.IncorrectValueError()
    elif args.password and not is_input_correct(args.password):
        raise errors.IncorrectValueError()
    else:
        response = models.app_user_settings_update(name, mail, password, owner)
    
    output.show_response(response)


@auth_controller_method
def user_profile_method(args):
    ''' Returns user profile.
    '''
    response = storage.user_select()
    
    output.show_user_profile(response)


@auth_controller_method
def logs_show_method(args):
    ''' Returns user/project logs.
    '''
    owner = storage.cache_get()
    project = args.project

    response = models.app_log_show_all(project, owner)    
    output.show_logs(response)


@auth_controller_method
def logs_clear_method(args):
    ''' Clears user/project logs.
    '''
    owner = storage.cache_get()
    project = args.project

    response = models.app_log_clear_all(project, owner)    
    output.show_response(response)


@auth_controller_method
def notification_show_method(args):
    ''' Returns user notifications.
    '''
    owner = storage.cache_get()
    response = models.app_notification_show_all(owner)
    
    output.show_notifications(response)


@auth_controller_method
def notification_clear_method(args):
    ''' Clears user notifications.
    '''
    owner = storage.cache_get()

    response = models.app_notification_clear_all(owner)    
    output.show_response(response)


def wrong_input_method(args):
    ''' Raises an MissedArgumentError, if user inputs not completed commands to console.
    '''
    try:
        raise errors.MissedArgumentError()
    except errors.tasQError as e:
        output.show_error_msg(e)


# validation funcs:
def is_input_correct(string):
    ''' Checks, if input is not too small
    '''
    if len(string) >= 3:
        return True
    else:
        return False
    
def is_mail_correct(string):
    ''' Checks, if mail is has correct format: mail@mail.mail
    '''
    regex = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

    if re.search(regex, string):
        return True
    else:
        return False

def is_bool_value_correct(value):
    ''' Checks, if user inputs the correct boolean value: 0/1
    '''
    if value == 0 or value == 1:
        return True
    else:
        return False

def is_datetime_correct(deadline):
    ''' Checks, if user inputs the correct deadline datetime in format dd/mm/yy hh:ii and if this datetime > datetime now.
    '''
    date_time = deadline.split("in")
    date = date_time[0].split("/")

    if len(date) == 3:
        for item in date:
            if not len(item.strip()) == 2:
                return False
    else:
        return False

    time = date_time[1].split(":")
    if len(time) == 2:
        for item in time:
            if not len(item.strip()) == 2:                
                return False
    else:
        return False

    datetime_object = datetime.datetime.strptime(deadline, '%d/%m/%y in %H:%M')
    if float(datetime_object.timestamp()) < float(datetime.datetime.now().timestamp()):
        return False
    else:
        return True

def is_period_correct(period):
    ''' Checks, if user inputs the correct task period value: 0..5
    '''
    if period >= 0 and period <= 5:
        return True
    else:
        return False

def is_priority_correct(priority):
    ''' Checks, if user inputs the correct task priority value: 0..3
    '''
    if priority >=0 and priority <= 3:
        return True
    else:
        return False