import tasQElib.controllers.storage_controller as storage
import tasQElib.controllers.models_controller as models
import tasQElib.models.user as UserClass
import tasQElib.models.session as SessionClass
import tasQElib.models.error as errors
import datetime

''' Auth Controller:
This module performs basic actions with user authorization.
You can login, register and end a session.

'''

def auth_check():
    ''' Returns True, if user auth. session isset.
    '''
    ID, Username = storage.cache_get()
    session = storage.session_select(ID)

    if session:
        if float(session.expire.timestamp()) >= float(datetime.datetime.now().timestamp()):
            return True
        else:
            return False 
    else:
        return False
        

def login(username, password):
    ''' Creates new user auth. session, if input arguments are correct.

    Arguments:
      username -- string value (required)
      password -- string value (required)

    '''
    if not auth_check():
        user = storage.user_select(username)

        if user:
            if user.password == password:
                new_session = SessionClass.Session(user.ID)
                storage.session_insert(new_session)
                storage.cache_update(user.ID, user.username)

                return True
            else:
                raise errors.WrongPasswordError()            
        else:
            raise errors.UserNotExistError()            
    else:
        raise errors.AlreadyAuthorizedError()   

def logout():
    ''' Deletes user auth. session.
    '''
    if auth_check():        
        ID, Username = storage.cache_get()
        storage.session_delete(ID)
        storage.cache_clear()

        return True
    else:
        return False

def registration(name, username, mail, password):
    ''' Creates user account.

    Arguments:
      name -- string value, means user name and surname (required)
      username -- string value, required for auth (required)
      mail -- string value (required)
      password -- string value (required)

    '''
    if not auth_check():
        user = storage.user_select(username)

        if user:
            raise errors.UserExistError()            
        else:
            new_user = UserClass.User(name, username, mail, password)
            storage.user_insert(new_user)

            return True            
    else:
        raise errors.AlreadyAuthorizedError()