import os

IS_LOGGING_ENABLED = True
IS_LOG_ALL = True
LOG_DIRECTORY = os.path.join(os.environ['HOME'], 'tasQE')
LOG_FILE = 'tasQE.log'
LOG_FORMAT = '%(asctime)s, %(name)s, [%(levelname)s]: %(message)s'