import console_core.controllers.input_controller as input
import console_core.logging_config as config
from tasQElib.logging import set_logging
import argparse
import os


''' Console View:
This module parse methods and arguments to work with the application from the console line.
The arguments are sent to input_controller.py

'''

def start():
    # set logging config:
    log_file = os.path.join(config.LOG_DIRECTORY, config.LOG_FILE)
    set_logging(is_enabled=config.IS_LOGGING_ENABLED, is_log_all=config.IS_LOG_ALL, log_file=log_file, log_format=config.LOG_FORMAT)

    # init parser:
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    parser.set_defaults(func=input.wrong_input_method)

    # command groups:
    project_group = subparsers.add_parser('project', help='[command group] This command group allows you to do smth with projects.')
    tasklist_group = subparsers.add_parser('tasklist', help='[command group]  This command group allows you to do smth with tasklists.')
    task_group = subparsers.add_parser('task', help='[command group]  This command group allows you to do smth with tasks.')
    notification_group = subparsers.add_parser('notifications', help='[command group]  This command group allows you to do smth with notifications.')
    user_group = subparsers.add_parser('user', help='[command group]  This command group allows you to do smth with your profile.')
    logs_group = subparsers.add_parser('logs', help='[command group]  This command group allows you to do smth with application logs [for super-users].')


    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # parser for AUTH commands:

    # registrtation:
    registration = subparsers.add_parser('registration', help='- This command allows you to make a registration. You need to be logout.')
    registration.add_argument('--name', type=str, required=True, help='- Your name; [required]')
    registration.add_argument('--username', type=str, required=True, help='- Should be unique; [required]')
    registration.add_argument('--password', type=str, required=True, help='- Your password; [required]')
    registration.add_argument('--mail', type=str, required=True, help='- E-mail should be in format: example@axample.com; [required]')
    registration.set_defaults(func=input.registration_method)

    # login:
    login = subparsers.add_parser('login', help='- This command allows you to login in application and get access to its functionality.')
    login.add_argument('--username', type=str, required=True, help='- Your username; [required]')
    login.add_argument('--password', type=str, required=True, help='- Your password; [required]')
    login.set_defaults(func=input.login_method)

    # logout:
    logout = subparsers.add_parser('logout', help='- This command allows you to logout out the application.')
    logout.set_defaults(func=input.logout_method)


    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # parser for USER commands:
    subparsers = user_group.add_subparsers()

    # change user settings:
    user_settings = subparsers.add_parser('settings', help='- This command allows you to change your user-account registration data.')
    user_settings.add_argument('--name', type=str, help='- Input new name;')
    user_settings.add_argument('--mail', type=str, help='- Input E-mail in format example@example.com;')
    user_settings.add_argument('--password', type=str, help='- Input new password;')
    user_settings.set_defaults(func=input.user_settings_method)

    # show user profile:
    user_profile = subparsers.add_parser('profile', help='- This command allows you to see your profile;')
    user_profile.set_defaults(func=input.user_profile_method)


    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # parser for LOGS commands:
    subparsers = logs_group.add_subparsers()

    # show logs:
    logs_show = subparsers.add_parser('show', help='- This command allows owner to see all user-logs of application.')
    logs_show.add_argument('--project', type=int, help='- ID of project, which logs you want to see;')
    logs_show.set_defaults(func=input.logs_show_method)

    # clear logs:
    logs_clear = subparsers.add_parser('clear', help='- This command allow super-user to clear user-logs of application.')
    logs_clear.add_argument('--project', type=int, help='- ID of project, which logs you want to clear;')
    logs_clear.set_defaults(func=input.logs_clear_method)


    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # parser for NOTIFICATION commands:
    subparsers = notification_group.add_subparsers()

    # show notifications:
    notification_show = subparsers.add_parser('show', help='- This command allow to see all your notifications.')
    notification_show.set_defaults(func=input.notification_show_method)

    # clear notifications:
    notification_clear = subparsers.add_parser('clear', help='- This command allow you to mark as read all your notofocations.')
    notification_clear.set_defaults(func=input.notification_clear_method)


    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # parser for PROJECT commands:
    subparsers = project_group.add_subparsers()

    # show all projects:
    project_all = subparsers.add_parser('all', help='- This command allows you to see all your projects.')
    project_all.set_defaults(func=input.project_all_method)

    # create project:
    project_create = subparsers.add_parser('create', help='- This command allows you to create new projects.')
    project_create.add_argument('--title', type=str, required=True, help='Title of project; [required]')
    project_create.add_argument('--color', type=int, help='Color of project.')
    project_create.set_defaults(func=input.project_create_method)

    # edit project:
    project_edit = subparsers.add_parser('edit', help='- This command allows you to edit your projects.')
    project_edit.add_argument('--id', type=int, required=True, help='- Select an id of project; [required]')
    project_edit.add_argument('--title', type=str, help='- Input new title;')
    project_edit.add_argument('--color', type=int, help='- Input new color;')
    project_edit.set_defaults(func=input.project_edit_method)

    # delete project:
    project_delete = subparsers.add_parser('delete', help='- This command allows you to delete your projects.')
    project_delete.add_argument('--id', type=int, required=True, help='- Select an id of project; [required]')
    project_delete.set_defaults(func=input.project_delete_method)

    # add members:
    project_members = subparsers.add_parser('members', help='- This command allows you to menage the members of all your projects.')
    subparsers = project_members.add_subparsers()
    project_members_add = subparsers.add_parser('add', help='- This command allows you to add a members to your project.')
    project_members_add.add_argument('--project', type=int, required=True, help='- Select an id of project; [required]')
    project_members_add.add_argument('--username', type=str, required=True, help='- Select a username; [required]')
    project_members_add.set_defaults(func=input.project_members_add_method)

    # show members:
    project_members_show = subparsers.add_parser('show', help='- This command allows you to see all members of your project.')
    project_members_show.add_argument('--project', type=int, required=True, help='- Select an id of project; [required]')
    project_members_show.set_defaults(func=input.project_members_method)

    # delete members:
    project_members_delete = subparsers.add_parser('delete')
    project_members_delete.add_argument('--project', type=int, required=True)
    project_members_delete.add_argument('--username', type=str, required=True)
    project_members_delete.set_defaults(func=input.project_members_delete_method)


    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # parser for TASKLIST commands:
    subparsers = tasklist_group.add_subparsers()

    # show all tasklists:
    tasklist_all = subparsers.add_parser('all', help='- This command allows you to see all your tasklists/project tasklists.')
    tasklist_all.add_argument('--project', type=int, help='- Select an id of project; [non-required]')
    tasklist_all.set_defaults(func=input.tasklist_all_method)

    # create tasklist:
    tasklist_create = subparsers.add_parser('create', help='- This command allows you to create new tasklists/project tasklists.')
    tasklist_create.add_argument('--title', type=str, required=True, help='- Input the title of tasklist; [required]')
    tasklist_create.add_argument('--project', type=int, help='- Input an id of project, if its a project tasklist;')
    tasklist_create.add_argument('--priority', type=int, default=0, help='- Input priority of tasklist;')
    tasklist_create.add_argument('--nextlist', type=int, help='- Input nextlist-rule, that will move task from relative tasklist to this on comlete;')
    tasklist_create.set_defaults(func=input.tasklist_create_method)

    # edit tasklist:
    tasklist_edit = subparsers.add_parser('edit', help='- This command allows you to edit your tasklists.')
    tasklist_edit.add_argument('--id', type=int, required=True, help='- Input an id of project; [required]')
    tasklist_edit.add_argument('--title', type=str, help='- Input a title of tasklist;')
    tasklist_edit.add_argument('--priority', type=int, default=0, help='- Input a priority of taslist;')
    tasklist_edit.add_argument('--nextlist', type=int, help='-  Input nextlist-rule, that will move task from relative tasklist to this on comlete;')
    tasklist_edit.set_defaults(func=input.tasklist_edit_method)

    # delete tasklist:
    tasklist_delete = subparsers.add_parser('delete', help='- This command allows you to delete your tasklists.')
    tasklist_delete.add_argument('--id', type=int, required=True, help='- Input an id of tasklist; [required]')
    tasklist_delete.set_defaults(func=input.tasklist_delete_method)

    # show tasklist:
    tasklist_show = subparsers.add_parser('show', help='- This command allows you to see all your tasklists.')
    tasklist_show.add_argument('--id', type=int, help='- Input an id of project, if its a project tasklist; [non-required]')
    tasklist_show.set_defaults(func=input.tasklist_show_method)


    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # parser for TASK commands:
    subparsers = task_group.add_subparsers()

    # create task:
    task_create = subparsers.add_parser('create', help='- This command allows you to create new task.')
    task_create.add_argument('--title', type=str, required=True, help='- Input a title; [required]')
    task_create.add_argument('--deadline', type=str, help='- Input a deadline in format dd/mm/yy in hh:mm;')
    task_create.add_argument('--period', type=int, default=0, help='- Input a preiod; [1; 5]')
    task_create.add_argument('--tasklist', type=int, help='- Input a relative tasklist;')
    task_create.add_argument('--desc', type=str, help='- Input a description;')
    task_create.add_argument('--priority', type=int, default=0, help='- Input a priority; [0; 3]')
    task_create.add_argument('--task', type=int, help='- Input a relative task id;')
    task_create.set_defaults(func=input.task_create_method)

    # edit task:
    task_edit = subparsers.add_parser('edit', help='- This command allows you to edit tasks.')
    task_edit.add_argument('--id', type=int, required=True, help='- Input an id of task; [required]')
    task_edit.add_argument('--title', type=str, help='- Input a new title of task;')
    task_edit.add_argument('--deadline', type=str, help='- Input a new deadline in format dd/mm/yy in hh:mm of task;')
    task_edit.add_argument('--period', type=int, help='- Input a new period of task; [1; 5]')
    task_edit.add_argument('--tasklist', type=int, help='- Input a new relative tasklist of task;')
    task_edit.add_argument('--desc', type=str, help='- Input a new description of task;')
    task_edit.add_argument('--priority', type=int, help='- Input a new priority of task; [0; 3]')
    task_edit.set_defaults(func=input.task_edit_method)
    
    # delete task:
    task_delete = subparsers.add_parser('delete', help='- This command allows you to delete tasks.')
    task_delete.add_argument('--id', type=int, required=True, help='- Input an id of task; [required]')
    task_delete.set_defaults(func=input.task_delete_method)

    # mark task as done:
    task_done = subparsers.add_parser('complete', help='- This command allows you to comlete tasks.')
    task_done.add_argument('--id', type=int, required=True, help='- Input an id of task; [required]')
    task_done.set_defaults(func=input.task_done_method)

    # unmark task as done:
    task_done = subparsers.add_parser('uncomplete', help='- This command allows you to uncomplete tasks.')
    task_done.add_argument('--id', type=int, required=True, help='- Input an id of task; [required]')
    task_done.set_defaults(func=input.task_undone_method)

    # show task:
    task_show = subparsers.add_parser('show', help='- This command allows you to see the task and its subtasks.')
    task_show.add_argument('--id', type=int, required=True, help='- Input an id of task; [required]')
    task_show.set_defaults(func=input.task_show_method)



    # start parse:
    args = parser.parse_args()
    args.func(args)