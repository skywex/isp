from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='tasQE Console',
    packages=find_packages(),
    install_requires=['peewee'],
    entry_points={
        'console_scripts': ['tasQE = console_core.view:start']
    }
)