import console.packages.view as console

''' tasQE, v.0.0.7:
A simple application to manage your tasks. What`s new?

+ user notifications on different actions;
+ second developing branch for Django;
+ library logger with console configuration;
+ web-interface for django;
+ user-project logs;
+ some fixes;

'''

def start():
    console.start()

if __name__ == "__main__":
    start()