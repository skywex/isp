# Start using tasQE! #
---

This is a simple application to manage your task plans. Application allows you to create your own tasklists, manage its priority and deadline. Also, with ***tasQE*** you can create period-tasks, which will automatically be repeated within the given time shift. 
To keep your tasks structured and not confused with others, you can combine them into large projects. For example, you can separate the tasks of developing software from the shopping list for a store. Also, you can break a large task into smaller sub-tasks, for more convenient interaction. The application allows you to use shared access to tasks and interact with other members of the project. So, you can develop a project together with your friends.

***tasQE*** has library and console versions. Application works on python3.

---

### ***Features*** ###

- Creating tasks with the ability to change the status of execution;
- Create custom categories that will contain tasks. Between categories, you can establish a link (rules) for automatically switching tasks from one to another;
- Typing tasks. Setting the time, priority, description, artist, repetition;
- Splitting the task into smaller sub-tasks. Subtasks will be small enough to not need a detailed description, because they can be issued in the form of a TODO sheet. Each sub-task can also be set by the performer, term, priority;
- Communication of performers within the task. The artist can leave messages in the task that will be visible to everyone. Messages are visible to all performers. The new singer will be able to read the entire message history. You can send a message to a specific user. The recipient will be notified about this through the system. It is allowed to attach images to a message;
- Control system: all notifications are displayed when the user is authorized. In notifications, there are records of upcoming / expired deadlines, user requests in the chat, changes in tasks where the current user is the owner;
- Two groups of users: project owner, project executors. The owner has full access to the entire project, you can add, edit, delete, view. Performers can view and edit only those tasks that they were assigned;
- Group tasks by date on the calendar tape. At will, all tasks can be displayed not as categories, but as a calendar;
- The priority of the task will be displayed in its color. This approach allows users to service quickly analyze the situation and understand what task it is now to take in the first place;
- Set the status of the task. When creating a task, it gets the status "In process". It can also be "Fulfilled" and "Expired";
- Deadlines. Each task can be assigned a deadline within which to set control points for the subtasks to which they should be executed (it is convenient for software development, where execution of one subtask allows other performers to start their work on a different goal). As the deadline approaches, the priority of the task will increase automatically. If the deadline came, and also not having performed, the system will mark it as overdue and wait for the confirmation of the user. Suddenly he just forgot to mark it done. Also, you can set the reminder time yourself. For example, this task is important - it is necessary to remind one day before the deadline. Another task is simpler and not so important - it is necessary to remind 20 minutes before deadline;
- Logging the action. In case the project is caught by unscrupulous performers, all their actions will be saved to the log. Log can be read only by the project owner;
- Subscription to the task. In the event that several executors are insisted, they automatically subscribe to its updates. The service will be checked by all subscribers about the status of its subtasks, the change of performers, commenting, etc. In the settings, each artist can mark the categories of interest to him.
- Authorization and registration of users;
- Dynamic lists. A separate screen where the outline and deadline dates are displayed will show the task lists for today and tomorrow.

---

### ***Installation*** ###

***tasQE*** has library and console versions. Library includes all activities about managing tasks. Console version — just a form of using tasQE library. You can use tasQE library to create your own apps.
So, lets install our application.

At first, make sure, that you already have setuptools:
```bash
$ pip3 install -U pip setuptools 
```

To install the library, input to console:
```bash
$ cd library
$ python3 setup.py install
```

To install the console application, input to console:
```bash
$ cd console
$ python3 setup.py install
```

### ***Interraction*** ###

After installation, you can use tasQE library in your own apps as module with different methods:
```bash
import tasQElib
```

If you want to have console-view interaction, you need to use special commands. All application is divided on PROJECT, TASKLIST, TASK, USER, LOGS, NOTIFICATION commands.
For example, to create your first project, use such command:
```bash
$ tasQE project create --title MyFirstProject
```

Use '-h' to see the documentation about each command.

---

``` © massertrozen, 2018 ```
